VolToy
======

* Simple Qt/QML volume data viewer.
* Originally intent was to do as much as possible in QML/OpenGL/GLSL domain.
    * Turned out to be limited by lack of QML support for 3D textures and while matrix4x4 is exposed in QML/js domain, you can't do much with it there.

See <TODO.md> for possible developments.

Building
========

Setup
-----

* In `data`, various make targets to download & unpack some DICOM (e.g `make PHENIX`).
* In `data-prep`, `./IMPORT.sh` to convert DICOM to monolithic, memory-mappable `.vol` files.  May need edit for whatever's actually been downloaded in data.

Compile
-------

Linux:

* `./MAKE-linux`

Windows:

* Opening `voltoy.pro` in QCreator seems to be the best option.
    * To run it, move the working directory up a level in the Run settings of Build&Run or it won't find QML files.

Running
=======

* `./build/voltoy` starts with default splash screen and file picker.
* `./build/voltoy qml/vol.qml?vol=vol/PHENIX0-361x512x512-0_700x0_424x0_424.vol` starts with given QML file and query parameters (last one used, if multiple).

Keyboard controls:
* PageUp/PageDown: move transverse slice
* Left/Right arrow keys: move sagittal slice
* Up/Down arrow keys: move coronal slice

In-view "toolbox":
* Home: reset to default view.
* Cine: start cineing in selected view's axis.

Header:
* Data: return to file picker screen.
* x: Quit VolToy.

Commandline options:
* -fullscreen, -maximized: these take effect during the splashscreen (seem to be problems with actually starting fullscreen in main.cpp).

Hosting
=======

How to get OpenGL on Windows EC2 instances?  (Working towards AppStream version)

* Has run on a g2.2xlarge: Nvidia's driver download page scan detects the grid driver is needed (178MByte install; sheesh!)
    * g2.2xlarge has GRID K520 GK104 "Kepler".
    * Need to do some desktop adjustment to move desktop onto the grid card; fortunately TeamViewer follows it!
* http://aws.amazon.com/blogs/aws/build-3d-streaming-applications-with-ec2s-new-g2-instance-type/ looks relevant.
    * Link above nores Can't use RDP; they recommend VNC or TeamViewer.
    * Teamviewer uses port 5938 on server.
* Local teamviewer (Debian): have to install the multiarch one; see the teamviewer Linux FAQ.

Installer
=========

* Qt installer framework just a tool for copying things into places.

* Aha: windeployqt ( http://qt-project.org/doc/qt-5/windows-deployment.html ) looks useful.

* qbs http://qt-project.org/wiki/QBS looks interesting.
    * Windows only (but that's what's wanted).
    * It has a wix module (MS' msi builder) http://qt-project.org/doc/qbs-1.3/wix-module.html although actual status/usability unclear.

