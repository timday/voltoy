setlocal
SET PATH=%PATH%;C:\Qt\Qt5.4.0rc\5.4\msvc2013_64_opengl\bin;C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin
mkdir deploy
mkdir deploy\qml
mkdir deploy\vol
copy build\voltoy.exe deploy\voltoy.exe /y /b
xcopy qml deploy\qml /s /e /y
xcopy vol deploy\vol /s /e /y
windeployqt --verbose 9 --release --qmldir qml --no-system-d3d-compiler deploy
