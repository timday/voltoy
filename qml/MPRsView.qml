import QtQuick 2.2

import VolToy 1.0

MPRs {
  id: view
  anchors.fill: parent
  anchors.margins: parent.outlineThickness
  textureID: volume.textureID
  transform: control.transformOblique

  wlWindow: control.wlWindow
  wlLevel: control.wlLevel

  Framerate {}

  VolToyMouseArea {}
}
