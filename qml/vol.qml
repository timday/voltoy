import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import VolToy 1.0

Rectangle {
  id: main
  color: style.background

  VolToyStyle {id: style}

  ImageDisplay {
    anchors.fill: parent
    image: browser.capture
    opacity: 1.0
    z: 2
    visible: (opacity!=0.0);
    Component.onCompleted: {opacity=0.0;}
    Behavior on opacity {PropertyAnimation {duration: 500;}}
  }

  About {
    id: about
    z: 1
  }

  Item {
    anchors.fill: main
    focus: true
    Keys.onPressed: {
      if (event.key==Qt.Key_Plus) {
        control.zoom*=1.125;event.accepted=true;
      } else if (event.key==Qt.Key_Minus) {
        control.zoom/=1.125;event.accepted=true;
      } else if (event.key==Qt.Key_PageUp) {
        control.step(Qt.vector3d(0,0,1));event.accepted=true;
      } else if (event.key==Qt.Key_PageDown) {
        control.step(Qt.vector3d(0,0,-1));event.accepted=true;
      } else if (event.key==Qt.Key_Left) {
        control.step(Qt.vector3d(1,0,0));event.accepted=true;
      } else if (event.key==Qt.Key_Right) {
        control.step(Qt.vector3d(-1,0,0));event.accepted=true;
      } else if (event.key==Qt.Key_Up) {
        control.step(Qt.vector3d(0,1,0));event.accepted=true;
      } else if (event.key==Qt.Key_Down) {
        control.step(Qt.vector3d(0,-1,0));event.accepted=true;
      }
    }
  }

  ViewControl {
    id: control
    anchors.fill: main
    dataSize: volume.dataSize
    voxelSize: volume.voxelSize

    property real wlLevelDefault: 1024
    property real wlWindowDefault: 1024

    property real wlLevel: wlLevelDefault
    property real wlWindow: wlWindowDefault

    property vector3d autocine: Qt.vector3d(0,0,0)
    property bool autocineing: !autocine.fuzzyEquals(Qt.vector3d(0,0,0))

    property real autorotate: 0.0
    property bool autorotating: (autorotate!=0.0)

    property bool mouseDragging: false

    property int mouseDragMode: ViewControl.MouseDragModePan

    Connections {
      target: (control.autocineing ? browser : null);
      onAnimationTick: {control.autocineStep();} 
    }

    Connections {
      target: (control.autorotating ? browser : null);
      onAnimationTick: {control.autorotateStep();} 
    }

/*
    Timer {
      interval: 1000.0/30.0
      running: control.autocineing
      repeat: true
      onTriggered: 
    }
    Timer {
      interval: 1000.0/30.0
      running: control.autorotating
      repeat: true
      onTriggered: {control.autorotateStep();}
    }
*/

    Behavior on zoom {
      enabled: (!resetanimation.running && !control.mouseDragging)
      PropertyAnimation {duration: 125;}
    }

    Behavior on focus {
      enabled: (!resetanimation.running && !control.mouseDragging && !control.autocineing && !control.autorotating)
      PropertyAnimation {duration: 125;}
    }

    function step(direction) {
      focus=focus.plus(direction.times(volume.voxelSize));
    }

    function autocineStep() {
      if (autocineing) {
        var f=focus.plus(autocine.times(voxelSize));
        if (containsPoint(f)) {
          focus=f;
        } else {
          var r=focus.minus(autocine.times(voxelSize));
          if (containsPoint(r)) {
            autocine=Qt.vector3d(0,0,0).minus(autocine);
            focus=r;
          } else {
            autocine=Qt.vector3d(0,0,0);
          }
        }
      }
    }

    function autorotateStep() {
      if (autorotating) {
        var r=rotation+autorotate;
        if (r<-180.0) {
          r=r+360.0;
        } else if (r>180.0) {
          r=r-360.0;
        }
        rotation=r;
      }
    }

    function reset(animated) {
      resetanimation.duration=(animated ? 500 : 0);
      resetanimation.running=true;
      control.mouseDragMode=ViewControl.MouseDragModePan;
    }

    SequentialAnimation {
      id: resetanimation
      property real duration: 0;
      running: false
      ScriptAction {
        script: {
          control.autocine=Qt.vector3d(0,0,0);
          control.autorotate=0.0;
        }
      }
      ParallelAnimation {
        PropertyAnimation {target: control;properties: 'focus';to: volume.centre;duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
        NumberAnimation {target: control;properties: 'zoom';to: 0.75*Math.min(2.0/volume.voxelSize.x,2.0/volume.voxelSize.y);duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
        NumberAnimation {target: control;properties: 'rotation';to: -40;duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
        NumberAnimation {target: control;properties: 'tilt';to: -30;duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
        NumberAnimation {target: control;properties: 'wlWindow';to: control.wlWindowDefault;duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
        NumberAnimation {target: control;properties: 'wlLevel';to: control.wlLevelDefault;duration: resetanimation.duration;easing.type: Easing.InOutQuad;}
      }
    }

    Component.onCompleted: {reset(false);}
  }
  
  Volume {
    id: volume
    // Must give it some size or doesn't ever sync
    width: 1
    height: 1
    source: browser.parameter('vol')
  }

  Item {
    id: header
    anchors.top: parent.top
    anchors.left: main.left
    anchors.right: main.right
    height: 0.075*parent.height

    RowLayout {
      anchors.fill: parent
      anchors.margins: 0.05*header.height

      VolToyButton {
        Layout.preferredHeight: 0.75*header.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/data3.svg'
        onClicked: {browser.captureScreenshot();browser.load('qml/index.qml');}
      }

      Item {
        Layout.preferredHeight: 0.75*header.height
        Layout.preferredWidth: Layout.preferredHeight
        Layout.fillWidth: true
  	Text {
          anchors.verticalCenter: parent.verticalCenter
          text: volume.description
          font.pixelSize: 0.5*parent.height
          font.family: 'Droid Sans'
  	  smooth: true
          color: style.passive
   	}
      }

      VolToyButton {
        Layout.preferredHeight: 0.75*header.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/expand42.svg'
        onClicked: browser.fullscreen();
      }

      VolToyButton {
        Layout.preferredHeight: 0.75*header.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/close47.svg'
        onClicked: Qt.quit();
      }
    }
  }

  VolToyHangingArea {
    id: views

    anchors.top: header.bottom
    anchors.bottom: footer.top
    anchors.left: main.left
    anchors.right: main.right
    anchors.margins: 2

    property bool showtools: false
    property bool showfps: false

    ViewOutline {
      objectName: 'transverseMPRView'
      property int hangingPosition: 0
      outlineColor: style.outlineTransverse

      MPRView {
	plane: MPR.Transverse
        transform: control.transformTransverse
      }

      VolToyToolbox {
        axis: Qt.vector3d(0,0,1);
      }
    }

    ViewOutline {
      objectName: 'coronalMPRView'
      property int hangingPosition: 1
      outlineColor: style.outlineCoronal

      MPRView {
	plane: MPR.Coronal
        transform: control.transformCoronal
      }

      VolToyToolbox {
        axis: Qt.vector3d(0,1,0);
      }
    }

    ViewOutline {
      objectName: 'sagittalMPRView'
      property int hangingPosition: 2
      outlineColor: style.outlineSagittal

      MPRView {
	plane: MPR.Sagittal
        transform: control.transformSagittal
      }

      VolToyToolbox {
        axis: Qt.vector3d(1,0,0);
      }
    }

    ViewOutline {
      objectName: 'obliqueMPRView'
      property int hangingPosition: 3
      outlineColor: style.outlineOther

      MPRView {
	plane: MPR.Oblique
        transform: control.transformOblique
      }

      VolToyToolbox {
        axis: control.axisOblique;
      }
    }

    ViewOutline {
      objectName: 'multipleMPRsView'
      property int hangingPosition: 4
      outlineColor: style.outlineOther
      z: -1

      MPRsView {}

      VolToyToolbox {
        axis: control.axisOblique;
      }
    }

    ViewOutline {
      objectName: 'volumeRenderedView'
      property int hangingPosition: 5
      outlineColor: style.outlineOther

      VRView {}

      VolToyToolbox {
        axis: control.axisOblique;
      }
    }
  }

  Item {
    id: footer
    anchors.left: main.left
    anchors.right: main.right
    anchors.bottom: parent.bottom
    height: 0.075*parent.height

    RowLayout {
      anchors.fill: parent
      anchors.margins: 0.05*footer.height

      VolToyButton {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/two313.svg'
        onClicked: {views.nextLayout();}
      }

      VolToyButton {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/four69.svg'
        onClicked: {views.rotateLayout();}
      }

      VolToyButton {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/home87.svg'
        onClicked: {views.resetLayout();}
      }

      VolToyToggle {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/screwdriver3.svg'
	active: views.showtools
        onClicked: {views.showtools=(!views.showtools);}
      }

      VolToyToggle {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        source: 'images/flaticon/speedometer26.svg'
	active: views.showfps
        onClicked: {views.showfps=(!views.showfps);}
      }

      Item {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: Layout.preferredHeight
        Layout.fillWidth: true
      }

      Item {
        Layout.preferredHeight: 0.75*footer.height
        Layout.preferredWidth: txt0.paintedWidth+2.0*glow.radius
  	Text {
          id: txt0
          anchors.fill: parent
          text: 'VolToy'
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          font.pixelSize: 0.5*parent.height
          font.family: 'Droid Sans'
          font.weight: Font.Bold
  	  smooth: true
          color: style.control
   	}
        Glow {
          id: glow
          anchors.fill: txt0
          cached: true
          radius: height*0.2
          spread: 0.5
          samples: 16
          color: style.controlGlow
          source: txt0
          opacity: 0.0
          Behavior on opacity { PropertyAnimation {duration: 250;} }
        }
        MouseArea {
          hoverEnabled: true
          onEntered: glow.opacity=1.0;
          onExited: glow.opacity=0.0;
          anchors.fill: parent
          onClicked: {about.opacity=1.0;}
        }
      }
    }
  }
}
