import QtQuick 2.2

Item {

  property int columnSpacing: 2
  property int rowSpacing: 2

  property bool showHangingPosition: false

  Component.onCompleted: {layout();}
  onWidthChanged: {layout();}
  onHeightChanged: {layout();}

  state: 'Layout2by3'

  function whichChildIsPosition(p) {
    for (var i=0;i<children.length;i++) {
      if (children[i].hangingPosition==p) {
        return i;
      }
    }
    return 0;
  }

  function demoteView(p) {
    var i=whichChildIsPosition(p);
    var j=whichChildIsPosition((p+5)%6);
    children[i].hangingPosition=(p+5)%6;
    children[j].hangingPosition=p;
    layout();
  }

  function promoteView(p) {
    var i=whichChildIsPosition(p);
    var j=whichChildIsPosition((p+1)%6);
    children[i].hangingPosition=(p+1)%6;
    children[j].hangingPosition=p;
    layout();
  }

  function zoomLeft(p) {
    var i=whichChildIsPosition(p);
    var j=whichChildIsPosition(2);
    children[i].hangingPosition=2;
    children[j].hangingPosition=p;    
    state='Layout4plus2'
    layout();
  }

  function zoomRight(p) {
    var i=whichChildIsPosition(p);
    var j=whichChildIsPosition(5);
    children[i].hangingPosition=5;
    children[j].hangingPosition=p;    
    if (state!='Layout4plus2') {
      state='Layout5plus1big';
    }
    layout();
  }

  function nextLayout() {
    if (state=='Layout2by3') {state='Layout5plus1';layout();}
    else if (state=='Layout5plus1') {state='Layout5plus1big';layout();}
    else if (state=='Layout5plus1big') {state='Layout4plus2';layout();}
    else if (state=='Layout4plus2') {state='Layout2by3';layout();}
  }

  function resetLayout() {
    for (var i=0;i<children.length;i++) {
      if (children[i].objectName=='transverseMPRView') children[i].hangingPosition=0;
      else if (children[i].objectName=='coronalMPRView') children[i].hangingPosition=1;
      else if (children[i].objectName=='sagittalMPRView') children[i].hangingPosition=2;
      else if (children[i].objectName=='obliqueMPRView') children[i].hangingPosition=3;
      else if (children[i].objectName=='multipleMPRsView') children[i].hangingPosition=4;
      else if (children[i].objectName=='volumeRenderedView') children[i].hangingPosition=5;
    }
    state='Layout2by3';
    layout();
    control.reset(true);
  }

  function layout() {
    if (state=='Layout2by3') layout2by3();
    else if (state=='Layout5plus1') layout5plus1();
    else if (state=='Layout5plus1big') layout5plus1big();
    else if (state=='Layout4plus2') layout4plus2();
  }

  function layout2by3() {
    var w=Math.max(0,(width -4*columnSpacing)/3.0);
    var h=Math.max(0,(height-3*rowSpacing   )/2.0);
    for (var i=0;i<children.length;i++) {
      children[i].x=columnSpacing+(w+columnSpacing)*Math.floor(children[i].hangingPosition%3);
      children[i].y=rowSpacing   +(h+   rowSpacing)*Math.floor(children[i].hangingPosition/3);
      children[i].width=w;
      children[i].height=h;
      children[i].visible=(w>0 && h>0);
    }
  }

  function layout5plus1() {
    var h=Math.max(0,(height-4*rowSpacing)/3.0);
    var w=Math.max(0,(width-4*columnSpacing)/3.0);
    for (var i=0;i<children.length;i++) {
      if (children[i].hangingPosition==5) {
        children[i].x=2*columnSpacing+w;
        children[i].y=rowSpacing;
        children[i].width=width-3*columnSpacing-w;
        children[i].height=height-3*rowSpacing-h;
      } else if (children[i].hangingPosition<2) {
        children[i].x=columnSpacing;
        children[i].y=rowSpacing+(h+rowSpacing)*Math.floor(children[i].hangingPosition);
        children[i].width=w;
        children[i].height=h;
      } else {
        children[i].x=columnSpacing+(children[i].hangingPosition-2)*(columnSpacing+w);
        children[i].y=rowSpacing+2*(h+rowSpacing);
        children[i].width=w;
        children[i].height=h;
      }
      children[i].visible=(children[i].width>0 && children[i].height>0);
    }
  }

  function layout5plus1big() {
    // Height of small and large views is automatic
    var h=Math.max(0,(height-6*rowSpacing)/5.0);
    // Want aspect ratio k such that W=k*(h+H)
    var k=width/(h+height);
    var w=k*h;
    for (var i=0;i<children.length;i++) {
      if (children[i].hangingPosition==5) {
        children[i].x=2*columnSpacing+w;
        children[i].y=rowSpacing;
        children[i].width=width-3*columnSpacing-w;
        children[i].height=height-2*rowSpacing;
      } else {
        children[i].x=columnSpacing;
        children[i].y=rowSpacing+(h+rowSpacing)*Math.floor(children[i].hangingPosition);
        children[i].width=w;
        children[i].height=h;
      }
      children[i].visible=(children[i].width>0 && children[i].height>0);
    }
  }

  function layout4plus2() {
    // Height of small and large views is automatic
    var h=Math.max(0,(height-3*rowSpacing)/2.0);
    var H=height-2*rowSpacing;
    // Want aspect ratio k such that W/2=k*(h+H)
    var k=0.5*width/(h+height);
    var w=k*h;
    var W=0.5*(width-5*columnSpacing-2*w);
    for (var i=0;i<children.length;i++) {
      if (children[i].hangingPosition==2) {
        children[i].x=2*columnSpacing+w;
        children[i].y=rowSpacing;
        children[i].width=W;
        children[i].height=H;
      } else if (children[i].hangingPosition==5) {
        children[i].x=4*columnSpacing+2*w+W;
        children[i].y=rowSpacing;
        children[i].width=W;
        children[i].height=H;
      } else {
        if (children[i].hangingPosition==0 || children[i].hangingPosition==1) {
          children[i].x=columnSpacing;
        } else {
          children[i].x=3*columnSpacing+w+W;
        }
        if (children[i].hangingPosition==0 || children[i].hangingPosition==3) {
          children[i].y=rowSpacing;
        } else {
          children[i].y=2*rowSpacing+h;
        }
        children[i].width=w;
        children[i].height=h;
      } 
      children[i].visible=(children[i].width>0 && children[i].height>0);
    }
  }

  function rotateLayout() {
    if (state=='Layout2by3') {
      var remap=[];
      remap[0]=1;
      remap[1]=2;
      remap[2]=5;
      remap[3]=0;
      remap[4]=3;
      remap[5]=4;
      for (var i=0;i<children.length;i++) {
        children[i].hangingPosition=remap[children[i].hangingPosition];
      }
    } else if (state=='Layout5plus1' || state=='Layout5plus1big') {
      var remap=[];
      remap[0]=5;
      remap[1]=0;
      remap[2]=1;
      remap[3]=2;
      remap[4]=3;
      remap[5]=4;
      for (var i=0;i<children.length;i++) {
        children[i].hangingPosition=remap[children[i].hangingPosition];
      }
    } else if (state=='Layout4plus2') {
      var remap=[];
      remap[0]=2;
      remap[1]=0;
      remap[2]=3;
      remap[3]=5;
      remap[4]=1;
      remap[5]=4;
      for (var i=0;i<children.length;i++) {
        children[i].hangingPosition=remap[children[i].hangingPosition];
      }
    }
    layout();
  }
}
