import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import VolToy 1.0

MouseArea {
  id: root

  anchors.fill: parent

  property vector3d axis: Qt.vector3d(0,0,0)

  property bool mouseover: false

  hoverEnabled: true
  onEntered: {mouseover=true;}
  onExited: {mouseover=false;}
  acceptedButtons: Qt.NoButton

  VolToyStyle {id: style;}

  Rectangle {
    anchors.fill: toolbox
    anchors.margins: -0.1*toolbox.height
    radius: 0.1*toolbox.height
    border.color: style.passive
    border.width: 0.125*radius
    color: style.panel
    opacity: toolbox.opacity
    smooth: true
  }

  GridLayout {
    id: toolbox
    x: 0.1*Math.min(parent.width,parent.height)
    y: x
    columns: 2
    property real preferredToolSize: 0.0875*Math.min(root.height,root.width)

    opacity: ((views.showtools || root.mouseover) ? 1.0 : 0.0)
    Behavior on opacity { PropertyAnimation {duration: 500;} }

    VolToyButton {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/calculation.svg'
      onClicked: {views.demoteView(parent.parent.parent.hangingPosition);}
      onEntered: views.showHangingPosition=true;
      onExited: views.showHangingPosition=false;
    }

    VolToyButton {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/plus81.svg'
      onClicked: {views.promoteView(parent.parent.parent.hangingPosition);}
      onEntered: views.showHangingPosition=true;
      onExited: views.showHangingPosition=false;
    }

    VolToyButton {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/up133.svg'
      reflectLeftRight: true
      onClicked: {views.zoomLeft(parent.parent.parent.hangingPosition);}
      onEntered: views.showHangingPosition=true;
      onExited: views.showHangingPosition=false;
    }

    VolToyButton {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/up133.svg'
      onClicked: {views.zoomRight(parent.parent.parent.hangingPosition);}
      onEntered: views.showHangingPosition=true;
      onExited: views.showHangingPosition=false;
    }

    VolToyButton {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/home87.svg'
      onClicked: {control.reset(true);}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/move5.svg'
      active: (control.mouseDragMode==ViewControl.MouseDragModePan);
      onClicked: {control.mouseDragMode=ViewControl.MouseDragModePan;}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/film40.svg'
      active: control.autocineing
      onClicked: {control.autocine=(control.autocineing ? Qt.vector3d(0,0,0) : root.axis);}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/two388.svg'
      active: (control.mouseDragMode==ViewControl.MouseDragModeCine);
      onClicked: {control.mouseDragMode=ViewControl.MouseDragModeCine;}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/clockwise2_with_play106.svg'
      active: control.autorotating
      onClicked: {control.autorotate=(control.autorotating ? 0.0 : -1.0);}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/triple17.svg'
      iconScale: 1.125
      active: (control.mouseDragMode==ViewControl.MouseDragModeRotate);
      onClicked: {control.mouseDragMode=ViewControl.MouseDragModeRotate;}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/contrast.svg'
      active: (control.mouseDragMode==ViewControl.MouseDragModeWindowLevel);
      onClicked: {control.mouseDragMode=ViewControl.MouseDragModeWindowLevel;}
    }

    VolToyToggle {
      Layout.preferredHeight: toolbox.preferredToolSize
      Layout.preferredWidth: toolbox.preferredToolSize
      source: 'images/flaticon/magnifying40.svg'
      active: (control.mouseDragMode==ViewControl.MouseDragModeZoom);
      onClicked: {control.mouseDragMode=ViewControl.MouseDragModeZoom;}
    }
  }

  Text {
    x: parent.width-toolbox.x-paintedWidth
    y: toolbox.y
    font.family: 'Droid Sans'
    font.pixelSize: 0.1*Math.min(root.height,root.width)
    text: (parent.parent.hangingPosition+1).toString();
    color: style.passive
    opacity: (views.showHangingPosition ? 1.0 : 0.0);
    visible: (opacity!=0.0);

    Behavior on opacity { PropertyAnimation {duration: 250;} }
  }
}
