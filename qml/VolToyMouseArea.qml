import QtQuick 2.2

import VolToy 1.0

MouseArea {
  anchors.fill: parent
  property real previous_x
  property real previous_y
  cursorShape: (control.mouseDragging ? Qt.OpenHandCursor : Qt.ClosedHandCursor)
  hoverEnabled: true

  function wrap(x) {
    if (x<-180.0) return x+360.0;
    if (x>180.0) return x-360.0;
    return x;
  }

  onPressed: {
    if (mouse.button==Qt.LeftButton) {
      control.mouseDragging=true;
      previous_x=mouse.x;
      previous_y=mouse.y;
      mouse.accepted=true;
    }
  }
  onPositionChanged: {
    if (mouse.buttons==Qt.LeftButton) {
      control.autocine=Qt.vector3d(0,0,0);
      control.autorotate=0.0;
      control.mouseDragging=true;
      var dx=(2.0*(mouse.x-previous_x))/Math.min(width,height);
      var dy=(2.0*(mouse.y-previous_y))/Math.min(width,height);

	if (control.mouseDragMode==ViewControl.MouseDragModePan) {
        control.focus=control.focus.plus(
          volume.normalizedDataToVolumeScaling.times(
            (
	        view.transform.times(Qt.vector3d(-dx,-dy,0))
	      ).minus(
	        view.transform.times(Qt.vector3d(0,0,0))
            )
          )
        );
      } else if (control.mouseDragMode==ViewControl.MouseDragModeRotate) {
        control.rotation=wrap(control.rotation+45.0*dx);
        control.tilt=wrap(control.tilt-45.0*dy);
      } else if (control.mouseDragMode==ViewControl.MouseDragModeZoom) {
        var m=dx-dy;
        control.zoom=control.zoom*Math.exp(m);
	} else if (control.mouseDragMode==ViewControl.MouseDragModeCine) {
        var m=dx-dy;
	  control.focus=control.focus.plus(
          volume.normalizedDataToVolumeScaling.times(
            (
	        view.transform.times(Qt.vector3d(0,0,m))
	      ).minus(
	        view.transform.times(Qt.vector3d(0,0,0))
            )
          )
        );
      } else if (control.mouseDragMode==ViewControl.MouseDragModeWindowLevel) {
        control.wlLevel=control.wlLevel+512*dy;
	  control.wlWindow=Math.max(0.0,control.wlWindow+512*dx);
      }
	previous_x=mouse.x;
      previous_y=mouse.y;
      mouse.accepted=true;
    }
  }
  onReleased: {
    if (mouse.button==Qt.LeftButton) {
      control.mouseDragging=false;
    }
  }
}
