import QtQuick 2.2

Item {
  FontLoader {
    source: "fonts/DroidSans.ttf"
    // Component.onCompleted: console.log('Loaded DroidSans.ttf to '+name);
  }
  FontLoader {
    source: "fonts/DroidSans-Bold.ttf"
    // Component.onCompleted: console.log('Loaded DroidSans-Bold.ttf to '+name);
  }
  FontLoader {
    source: "fonts/DroidSansMono.ttf"
    // Component.onCompleted: console.log('Loaded DroidSansMono.ttf to '+name);
  }
}
