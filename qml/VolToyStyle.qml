import QtQuick 2.2

Item {

  property color background:  '#000000'

  property color passive:     '#ffff00'

  property color control:     '#ff8800'
  property color controlGlow: '#ffcc00'

  property color active:      '#00ff00'
  property color activeGlow:  '#88ff00'

  property color panel:       '#cc000000'

  property color outlineTransverse: '#0000ff'
  property color outlineCoronal:    '#00ff00'
  property color outlineSagittal:   '#ff0000'
  property color outlineOther:      '#cccc00'

  VolToyFonts {}
}
