import QtQuick 2.2
import QtGraphicalEffects 1.0

Item {
  id: root

  property string source
  property bool active: false
  property real iconScale: 1.0

  signal clicked

  Rectangle {
    id: icon
    anchors.fill: parent
    color: '#00000000'
    visible: false
    Image {
      anchors.fill: parent
      anchors.margins: 0.0625*parent.height
      source: root.source
      sourceSize.width: width
      sourceSize.height: height
      smooth: true
      transformOrigin: Item.Bottom
      scale: root.iconScale
    }
  }

  ShaderEffectSource {
    id: iconsrc
    anchors.fill: parent
    sourceItem: icon
  }

  VolToyTint {
    id: colored
    anchors.fill: iconsrc
    source: iconsrc
    tint: (root.active ? style.active : style.control)
  }

  Glow {
    id: glow
    anchors.fill: parent
    source: colored
    cached: true
    radius: height*0.2
    spread: 0.5
    samples: 16 
    color: (root.active ? style.activeGlow : style.controlGlow)
    opacity: 0.0
    Behavior on opacity { PropertyAnimation {duration: 250;} }
  }
  MouseArea {
    anchors.fill: parent
    hoverEnabled: true
    onClicked: root.clicked();
    onEntered: glow.opacity=1.0;
    onExited: glow.opacity=0.0;
  }
}
