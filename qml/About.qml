import QtQuick 2.2
import QtQuick.Controls 1.3

import VolToy 1.0

Rectangle {
  id: about

  anchors.fill: main
  anchors.margins: 0.05*Math.min(main.width,main.height)

  color: style.panel

  VolToyStyle {id: style}

  radius: 0.1*height
  border.color: style.passive
  border.width: 0.125*radius
  smooth: true

  opacity: 0.0
  visible: (opacity>0.0)

  Behavior on opacity { PropertyAnimation {duration: 500;} }

  MouseArea {
    anchors.fill: parent
  }

  Item {
    
    anchors.fill: parent
    anchors.margins: 0.5*parent.radius

    ScrollView {
      id: scrollview
      anchors.top: closebutton.bottom
      anchors.bottom: parent.bottom
      anchors.left: parent.left
      anchors.right: closebutton.left
      verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
      horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
      frameVisible: true

      MouseArea {
        anchors.fill: parent
      }
      Text {
        id: abouttext
        width: scrollview.viewport.width
        text: browser.fileContents('qml/about.html')
        textFormat: Text.RichText
	wrapMode: Text.Wrap
        font.family: 'Droid Sans'
        smooth: true
        color: style.passive
        onLinkActivated: {console.log('Clicked link '+link);browser.openInExternalBrowser(link);}
      }
    }

    VolToyButton {
      id: closebutton
      anchors.top: parent.top
      anchors.right: parent.right
      height: 0.075*parent.height
      width: height
      source: 'images/flaticon/close47.svg'
      onClicked: {about.opacity=0.0;}
    }
  }
}
