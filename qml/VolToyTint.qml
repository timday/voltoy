import QtQuick 2.2

ShaderEffect {
  property variant source: nil
  property color tint: '#ff0000'
  vertexShader: "
    uniform highp mat4 qt_Matrix;
    attribute highp vec4 qt_Vertex;
    attribute highp vec2 qt_MultiTexCoord0;
    varying highp vec2 coord;
    void main() {
        coord = qt_MultiTexCoord0;
        gl_Position = qt_Matrix * qt_Vertex;
    }"
  fragmentShader: "
    varying highp vec2 coord;
    uniform highp float qt_Opacity;
    uniform sampler2D source;
    uniform highp vec4 tint;
    void main() {
      highp vec4 v = texture2D(source,coord);
      gl_FragColor = qt_Opacity*v.a*tint;
   }"
}
