import QtQuick 2.2
import QtGraphicalEffects 1.0

Rectangle {
  id: main
  color: style.background

  VolToyStyle {id: style}

  Item {
    anchors.top: parent.top
    anchors.bottom: title.top
    anchors.left: parent.left
    anchors.right: parent.right
    Image {
      id: logo
      anchors.verticalCenter: parent.verticalCenter
      anchors.horizontalCenter: parent.horizontalCenter
      source: 'images/flaticon/skeleton.svg'
      height: 0.9*parent.height
      width: height
      sourceSize.width: width
      sourceSize.height: height
      fillMode: Image.PreserveAspectFit
      asynchronous: true
      opacity: 0.0      
    }

    VolToyTint {
      anchors.fill: logo
      source: logo
      tint: style.control
    }
  }

  Text {
    id: title
    anchors.bottom: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    text: 'VolToy'
    horizontalAlignment: Text.AlignHCenter
    font.family: 'Droid Sans'
    font.weight: Font.Bold
    font.pixelSize: 0.1*main.height
    color: style.passive
    opacity: 0.0
  }

  Text {
    id: attribution
    anchors.top: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
    text: 'timday.com'
    horizontalAlignment: Text.AlignHCenter
    font.family: 'Droid Sans'
    font.pixelSize: 0.05*main.height
    color: style.passive
    opacity: 0.0
  }

  Item {
    anchors.top: attribution.bottom
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
 
    Image {
      id: qtlogo
      anchors.verticalCenter: parent.verticalCenter
      anchors.horizontalCenter: parent.horizontalCenter
      source: 'images/qt-logo.svg'
      height: 0.5*parent.height
      width: height
      sourceSize.width: width
      sourceSize.height: height
      fillMode: Image.PreserveAspectFit
      asynchronous: true
      opacity: 0.0
    }
  }

  SequentialAnimation {
    running: true
    PropertyAnimation {target:logo;property:'opacity';to:1.0;duration: 250}
    PropertyAnimation {target:title;property:'opacity';to:1.0;duration: 250}
    PropertyAnimation {target:attribution;property:'opacity';to:1.0;duration: 250}
    PropertyAnimation {target:qtlogo;property:'opacity';to:1.0;duration: 250}
    ScriptAction {
      script: {
        if (browser.options.indexOf("-fullscreen")!=-1) browser.showFullScreen();
        else if (browser.options.indexOf("-maximized")!=-1) browser.showMaximized();
      }
    }
    PauseAnimation {duration: 250}
    ScriptAction {
      script: {browser.captureScreenshot();browser.load('qml/index.qml');}
    }
  }
}
