import QtQuick 2.2

Item {
  property color outlineColor
  property real outlineThickness: 1

  Behavior on x { PropertyAnimation {duration: 500;easing.type: Easing.InOutQuad;} }
  Behavior on y { PropertyAnimation {duration: 500;easing.type: Easing.InOutQuad;} }
  Behavior on width { PropertyAnimation {duration: 500;easing.type: Easing.InOutQuad;} }
  Behavior on height { PropertyAnimation {duration: 500;easing.type: Easing.InOutQuad;} }

  Rectangle {
    anchors.top: parent.top
    height: parent.outlineThickness
    width: parent.width
    color: parent.outlineColor
  }

  Rectangle {
    anchors.bottom: parent.bottom
    height: parent.outlineThickness
    width: parent.width
    color: parent.outlineColor
  }

  Rectangle {
    anchors.left: parent.left
    height: parent.height
    width: parent.outlineThickness
    color: parent.outlineColor
  }

  Rectangle {
    anchors.right: parent.right
    height: parent.height
    width: parent.outlineThickness
    color: parent.outlineColor
  }
}
