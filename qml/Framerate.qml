import QtQuick 2.2

Text {

  id: fps
  visible: views.showfps
  anchors.bottom: parent.bottom
  anchors.right: parent.right
  anchors.rightMargin: 0.025*parent.width
  anchors.bottomMargin: 0.025*parent.height
  width: paintedWidth
  height: paintedHeight
  text: '0.00 FPS'
  font.pixelSize: 0.05*parent.height
  font.family: 'Droid Sans Mono'
  smooth: true
  color: style.passive
  opacity: 0.0
  Behavior on opacity { PropertyAnimation {duration: 2000;} }

  property var events: []

  function event() {
    fadetimer.restart();
    opacity=1.0;
    events.push((new Date()).getTime())
  }

  function report() {
    var t=(new Date()).getTime();
    while (events.length>0 && events[0]<t-2000) {
      events.shift();
    }
    if (events.length<=2) {
      text='? FPS';
    } else {
      text=((events.length-1)/(0.001*(events[events.length-1]-events[0]))).toFixed(1)+" FPS";
    }
  }

  Connections {
    target: parent  // Assumed to be child of an MPR or similar source of frameEvent()s
    onFrameEvent: {fps.event();}
  }

  Timer {
    id: fadetimer
    interval: 2000
    onTriggered: {fps.opacity=0.0;}
  }

  Timer {
    interval: 1000
    repeat: true
    running: true
    onTriggered: {fps.report();}
  }

  VolToyStyle {
    id: style
  }
}
