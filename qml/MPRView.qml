import QtQuick 2.2

import VolToy 1.0

MPR {
  id: view
  anchors.fill: parent
  anchors.margins: parent.outlineThickness
  outlineColor: parent.outlineColor
  textureID: volume.textureID

  wlWindow: control.wlWindow
  wlLevel: control.wlLevel

  // Includer expected to do:
  //   plane: MPR.Transverse
  //   matrix4x4 transform: control.transformTransverse

  Framerate {}

  VolToyMouseArea {}
}
