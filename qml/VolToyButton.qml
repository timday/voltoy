import QtQuick 2.2
import QtGraphicalEffects 1.0

MouseArea {
  id: root

  property string source
  property bool reflectLeftRight: false

  hoverEnabled: true
  onEntered: glow.opacity=1.0;
  onExited: glow.opacity=0.0;

  Rectangle {
    id: icon
    anchors.fill: parent
    color: '#00000000'
    visible: false
    Image {
      id: image
      anchors.fill: parent
      anchors.margins: 0.0625*parent.height
      source: root.source
      fillMode: Image.PreserveAspectFit
      sourceSize.width: width
      sourceSize.height: height
      smooth: true
      transform: [
        Scale {
          origin.x: 0.5*image.width
          origin.y: 0.0
          xScale: (root.reflectLeftRight ? -1.0 : 1.0);
          yScale: 1.0
        }
      ]
    }
  }

  ShaderEffectSource {
    id: iconsrc
    anchors.fill: parent
    sourceItem: icon
  }

  VolToyTint {
    id: colored
    anchors.fill: iconsrc
    source: iconsrc
    tint: style.control
  }

  Glow {
    id: glow
    anchors.fill: parent
    source: colored
    radius: height*0.2
    spread: 0.5
    samples: 16
    cached: true
    color: style.controlGlow
    opacity: 0.0
    Behavior on opacity { PropertyAnimation {duration: 250;} }
  }
}
