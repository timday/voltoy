import QtQuick 2.4
import QtQuick.Controls 1.3
import Qt.labs.folderlistmodel 2.1
import QtGraphicalEffects 1.0

import VolToy 1.0

Rectangle {
  id: main
  color: style.background

  VolToyStyle {id: style}

  ImageDisplay {
    anchors.fill: parent
    image: browser.capture
    opacity: 1.0
    z: 1
    visible: (opacity!=0.0);
    Component.onCompleted: {opacity=0.0;}
    Behavior on opacity {PropertyAnimation {duration: 500;}}
  }

  ScrollView {
    id: scrollview
    anchors.fill: main
    verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
    horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

    ListView {
      id: list
      width: scrollview.viewport.width
      anchors.margins: 0.01*main.height
    
      spacing: 0.01*main.height
    
      populate: Transition {
        NumberAnimation {properties: 'x,y';duration: 1000;}
      }
    
      FolderListModel {
        id: folderModel
        folder: '../vol/'
        nameFilters: ['*.vol']
        showDirs: false
        showDotAndDotDot: false
      }
    
      Component {
        id: fileDelegate
        Rectangle {
          anchors.margins: -0.1*height
          width: list.width
          height: main.height/Math.max(10,folderModel.count)
          radius: 0.1*height
          border.color: style.passive
          border.width: Math.max(1.0,0.25*radius)
          color: style.panel
    	  smooth: true
    
          VolToyStyle {id: style}
    
          Row {
            id: listing
            height: parent.height
            Item {
              id: ready
              width: parent.height
              height: parent.height
              opacity: 0.0
              NumberAnimation {
                id: fade
                target: ready
                property: "opacity"
                from: 1.0
                to: 0.0
                duration: 1000
              }
              Rectangle {
                anchors.fill: parent
                color: '#00000000'
                Image {
                  id: readyicon
                  anchors.fill: parent
                  anchors.margins: 0.125*parent.height
                  fillMode: Image.PreserveAspectFit
                  source: 'images/flaticon/data3.svg'
                  sourceSize.width: width
                  sourceSize.height: height
                  visible: false
                  smooth: true
                }
                VolToyTint {
                  anchors.fill: readyicon
                  source: readyicon
                  tint: style.passive
                }
              }
              Component.onCompleted: browser.touchFile('vol/'+txt.text);
              Connections {
                target: browser
                onTouchedFile: {if (filename=='vol/'+txt.text) {ready.opacity=1.0;fade.running=true;}}
              }
            }
            Item {
              id: spacer
              width: parent.height
              height: width
            }
            Item {
              id: label
              height: parent.height
              width: listing.parent.width-x-0.5*spacer.width
              Text {
                id: txt
                text: fileName
                width: parent.width
                height: parent.height
    	        verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                font.pixelSize: 0.5*parent.height
                font.family: 'Droid Sans'
                smooth: true
                color: style.control
                elide: Text.ElideRight
              }
              Glow {
                id: glow
                anchors.fill: label
                cached: true
                radius: height*0.2
                spread: 0.5
                samples: 16
                color: style.controlGlow
                source: txt
                opacity: 0.0
                Behavior on opacity { PropertyAnimation {duration: 250;} }
              }
              MouseArea {
                anchors.fill: label
                hoverEnabled: true
                onEntered: {glow.opacity=1.0;browser.cancelAllTouchFiles();browser.touchFile('vol/'+txt.text);}
                onExited: glow.opacity=0.0;
                onClicked: {
                  console.log('Selected: '+txt.text);
                  browser.captureScreenshot();
                  browser.load('qml/vol.qml?vol=../vol/'+txt.text);
                }
              } 
            }
          }        
        }
      }
    
      Component {
        id: headerDelegate
        Text {
          text: 'Select a volume:'
          font.pixelSize: 0.05*main.height
          font.family: 'Droid Sans'
          font.weight: Font.Bold
          smooth: true
          color: style.passive
          VolToyStyle {id: style}
        }
      }
    
      Component {
        id: footerDelegate
        Text {
          text: ' '
          font.pixelSize: 0.05*main.height
          font.family: 'Droid Sans'
          font.weight: Font.Bold
          smooth: true
          color: style.passive
          VolToyStyle {id: style}
        }
      }
    
      model: folderModel
      delegate: fileDelegate
      header: headerDelegate
      footer: footerDelegate
    }
  }
}
