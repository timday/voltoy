VolToy TODO
===========

Sooner
------

* On autocine/autorotate ticks, get hold of actual elapsed time to get smoother motion.
* VR view
    * SVR
    * VR view has a gimballed orientation about the point of interest.  Mouse control could be better.
    * Optional niche-clip for VR view.
* OpenGL refactoring, scoped helpers.
    * OpenGL thick smooth lines enable/disable; refactor into a helper.
    * Depth buffer state; bit confused what state it's in by default; maybe should be got/restored.
* Show all planes as overlays on axial MPRs (would yield coloured crosshairs).  Could/should show oblique too?
* Scrollwheel should always zoom.
* Shift modifier keys should modify toolbox active tool.
* Toolbox cine ability to reverse direction?  Either go opposite to last one, or double click?
* Toolbox toggles: active colour should animate?
* Cursor shape; doesn't work or just doesn't work on Linux?  Doesn't work on Mac either.  Ah, needs hoverEnabled: true?  Test.  No, that's not it.
* AppStream deployment
* Installer?
  * http://qt-project.org/wiki/Qt-Installer-Framework
  * http://download.qt-project.org/snapshots/ifw/
    * Needs to include qml and vol folders; and some WINQUIRKS to set current directory to nearby (and probably index.qml's file picker to vol/).
* Keyboard controls.
    * Zoom and focus is animated with QML BehaviorOn... but what else is possible?
* Slab.
    * With some plane overlays/cross-hairs expanding to show extent.
* Use QOpenGLTexture to tidy up volume texture code.

* File browser.
  * Thumbnails.
  * Make filebrowser dialog accessible from button (useful for navigation to other dirs).

* Build it on windows and run it on AppStream.
    * Might be useful to be able to run it "normally" on EC2 while building it, but that'll have OpenGL-RDP troubles; see <https://forums.aws.amazon.com/thread.jspa?messageID=500601>

Later
-----
* More arcball like control for rotation on 3D views?  Maybe more for a touch UI.
* Mac build: FileDialog not working?  Yes: https://bugreports.qt-project.org/browse/QTBUG-31699 has QML workround.  But all-QML solution works better; maybe FileDialog useful for navigating to another folder.

Long term
---------
* A *lot* more would be doable in QML-domain GLSL *if* there was some way of getting a sampler3d into a ShaderEffect.
    * http://qt-project.org/forums/viewthread/15348.
    * I note QOpenGLTexture http://qt-project.org/doc/qt-5/qopengltexture.html includes a QOpenGLTexture::BindingTarget3D.  Question is whether one could be exposed as a property in some way that it could be got into a QML ShaderEffect.

Info
----
* Nice cc-licensed flat-look icons at http://www.flaticon.com/categories/ 
