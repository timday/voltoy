TEMPLATE = app

TARGET = voltoy

#CONFIG += debug
CONFIG += release

CONFIG += c++11
CONFIG += precompile_header

QT += core gui quick concurrent svg xml

SOURCES += $$files(src/*.cpp)
HEADERS += $$files(src/*.h)
PRECOMPILED_HEADER = src/precompiled.h

DESTDIR = build
OBJECTS_DIR = build/obj
MOC_DIR = build/moc

unix:!mac{
  DEFINES += LINUXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11
}

mac {
  BUNDLE_QML.path = qml/
  BUNDLE_QML.files = $$files(qml/*.qml)
  BUNDLE_QML.files += $$files(qml/*.js)
  QMAKE_BUNDLE_DATA += BUNDLE_QML

  BUNDLE_FONTS.path = qml/fonts/
  BUNDLE_FONTS.files = $$files(qml/fonts/*.ttf)
  QMAKE_BUNDLE_DATA += BUNDLE_FONTS

  BUNDLE_IMAGES.path = qml/images/
  BUNDLE_IMAGES.files = $$files(qml/images/*.png)
  BUNDLE_IMAGES.files = $$files(qml/images/*.svg)
  QMAKE_BUNDLE_DATA += BUNDLE_IMAGES

  BUNDLE_FLATICON.path = qml/images/flaticon/
  BUNDLE_FLATICON.files = $$files(qml/images/flaticon/*.svg)
  BUNDLE_FLATICON.files += $$files(qml/images/flaticon/*.png)
  QMAKE_BUNDLE_DATA += BUNDLE_FLATICON

  BUNDLE_VOL.path = vol/
  BUNDLE_VOL.files = $$files(vol/*.vol)
  QMAKE_BUNDLE_DATA += BUNDLE_VOL
}

macx {
  DEFINES += MACXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11
  QMAKE_MACOSX_DEPLOYMENT_TARGET=10.9
}

win32 {
  DEFINES += WINQUIRKS
}

ios {
  DEFINES += IOSQUIRKS
}

cache()
