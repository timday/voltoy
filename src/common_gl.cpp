#include "common_gl.h"

void glerrcheck(GLFns& gl,const char* where) {
#ifdef QT_DEBUG
  const GLenum error=gl.glGetError();
  if (error!=GL_NO_ERROR) {
    std::cerr << where << ": gl error 0x" << std::hex << error << std::dec << " detected" << std::endl;
  }
#endif
}
