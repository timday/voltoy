#include "VolumeItemRenderer.h"

#include "VolumeItem.h"

VolumeItemRenderer::VolumeItemRenderer()
  :_texture_id(0)
{
  if (!_gl.initializeOpenGLFunctions()) {
    std::cerr << "VolumeItemRenderer ctor: initializeOpenGLFunctions failed" << std::endl;
  }

  glerrcheck(_gl,"VolumeItemRenderer ctor");
}

VolumeItemRenderer::~VolumeItemRenderer()
{
  if (_texture_id) {
    _gl.glDeleteTextures(1,&_texture_id);
  }
}

void VolumeItemRenderer::render()
{
  glerrcheck(_gl,"VolumeItemRenderer::render");
}

void VolumeItemRenderer::synchronize(QQuickFramebufferObject* item) {
  glerrcheck(_gl,"VolumeItemRenderer::synchronize start");

  if (!_texture_id) {
    _gl.glGenTextures(1,&_texture_id);
  }

  VolumeItem*const volume_item=dynamic_cast<VolumeItem*>(item);
  if (!volume_item) {
    std::cerr << "Unexpected argument to VolumeItemRenderer::synchronize" << std::endl;
  }

  if (volume_item->changedForRenderer()) {
    _gl.glEnable(GL_TEXTURE_3D);
    
    _gl.glBindTexture(GL_TEXTURE_3D,_texture_id);
    _gl.glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    _gl.glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    _gl.glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
    _gl.glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
    _gl.glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_BORDER);

    std::array<float,4> border_colour={{0.0f,0.0f,0.0f,0.0f}};
    _gl.glTexParameterfv(GL_TEXTURE_3D,GL_TEXTURE_BORDER_COLOR,&border_colour[0]);

    _gl.glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    
    _gl.glTexImage3D(
      GL_TEXTURE_3D,
      0,
      GL_R16,
      volume_item->dataWidth(),
      volume_item->dataHeight(),
      volume_item->dataDepth(),
      0,
      GL_RED,
      GL_UNSIGNED_SHORT,
      volume_item->data()
    );
    
    _gl.glBindTexture(GL_TEXTURE_3D,0);
    _gl.glDisable(GL_TEXTURE_3D);

    volume_item->setChangedForRenderer(false);
    volume_item->setTextureID(_texture_id);
    std::cerr << "#";  
  }

  glerrcheck(_gl,"VolumeItemRenderer::synchronize end");
}
