#ifndef __voltoy_vritemrenderer_h__
#define __voltoy_vritemrenderer_h__

#include "MPRItemRendererBase.h"
#include "VRItem.h"

class VRItemRenderer : public MPRItemRendererBase {

public:

  VRItemRenderer();
  ~VRItemRenderer();

  void synchronize(QQuickFramebufferObject*);

  void render();
  
private:

  QOpenGLShaderProgram _program_vr;

  QVector3D _normalizedDataToVolumeScaling;
};

#endif
