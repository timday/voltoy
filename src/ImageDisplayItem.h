#ifndef __voltoy_imagedisplay_h__
#define __voltoy_imagedisplay_h__

class ImageDisplayItem : public QQuickPaintedItem {
  
  Q_OBJECT

  Q_PROPERTY(QImage image READ image WRITE setImage NOTIFY imageChanged)

public:

  ImageDisplayItem(QQuickItem* parent=0);
  ~ImageDisplayItem();

  const QImage& image() const;
  void setImage(const QImage&);

  void paint(QPainter*);

signals:

  void imageChanged(const QImage&);

private:

  QImage _image;
};

#endif
