#include "VolumeItem.h"

#include "force.h"
#include "VolumeItemRenderer.h"

namespace {

  // Touch all the pages in a range to bring them into RAM ASAP.
  void scan(const ushort* ptr,size_t n) {
    ushort lo=*ptr;
    ushort hi=*ptr;
    for (size_t i=1;i<n;++i,++ptr) {
      if (*ptr<lo) lo=*ptr;
      if (*ptr>hi) hi=*ptr;
    }
    force(hi-lo);
    std::cerr << "Scanned " << std::fixed << std::setprecision(1) << n/float(1<<20) << " Megabytes; range " << lo << " - " << hi << std::endl;
  }
}

VolumeItem::VolumeItem(QQuickItem* parent)
  :QQuickFramebufferObject(parent)
  ,_ok(false)
  ,_changed_for_renderer(false)
  ,_data(0)
  ,_data_width(0)
  ,_data_height(0)
  ,_data_depth(0)
  ,_texture_id(0)
  ,_description("No data")
{}

VolumeItem::~VolumeItem() {
}

QUrl VolumeItem::source() const {
  return _source;
}

void VolumeItem::setSource(const QUrl& source) {
  if (source!=_source) {

  _source=source;
  emit sourceChanged(_source);

  std::cerr << "VolumeItem set to source: " << source.toString().toLocal8Bit().data() << std::endl;

  cleanup();

    if (_source.isLocalFile()) {

      const QString filename=_source.fileName();

      std::cerr << "VolumeItem::setSource filename is: " << filename.toLocal8Bit().data() << std::endl;

      // Expected format is <name>-<slices>x<rows>x<cols>-<voxel_z>x<voxel_y>x<voxel_x>.vol

      const QString filename_base=filename.split(".")[0];
  
      const QStringList parts=filename_base.split("-");
      
      {
	const QStringList sizes=parts[1].split("x");
	_data_width=sizes[2].toInt();
	_data_height=sizes[1].toInt();
	_data_depth=sizes[0].toInt();
      }

      {
	QStringList voxel_sizes=parts[2].split("x");
	for (int i=0;i<voxel_sizes.length();++i) {
	  voxel_sizes[i].replace('_','.');
	}
	_voxel_size=QVector3D(voxel_sizes[2].toFloat(),voxel_sizes[1].toFloat(),voxel_sizes[0].toFloat());
      }

      emit dataSizeChanged(dataSize());
      emit voxelSizeChanged(voxelSize());
      emit normalizedDataToVolumeScalingChanged(normalizedDataToVolumeScaling());
      emit centreChanged(centre());

      if (_data_width>0 && _data_height>0 && _data_depth>0) {    
	_file.reset(new QFile(_source.toLocalFile()));
	if (_file->open(QIODevice::ReadOnly)) {
	  _data=_file->map(0,_file->size());
	  if (_data) {
	    std::cerr << "Volume file " << _file->fileName().toLocal8Bit().data() << " mapped" << std::endl;
	    _preload=QtConcurrent::run(scan,reinterpret_cast<const ushort*>(_data),_file->size()/sizeof(ushort));
	    _ok=true;
	  }
	} else {
	  setDescription(QString("Failed to open file ")+_file->fileName());
	}
      } else {
        setDescription(QString("Failed to determine dimensions of file ")+_file->fileName());
      }
    }
    setDescription(QString("File: ")+_file->fileName());
    setChangedForRenderer(true);
  }
}

bool VolumeItem::changedForRenderer() const {
  return _changed_for_renderer;
}

void VolumeItem::setChangedForRenderer(bool v) {
  _changed_for_renderer=v;
}

const uchar* VolumeItem::data() const {
  return _data;
}

uint VolumeItem::dataWidth() const {
  return _data_width;
}

uint VolumeItem::dataHeight() const {
  return _data_height;
}

uint VolumeItem::dataDepth() const {
  return _data_depth;
}

QVector3D VolumeItem::dataSize() const {
  return QVector3D(_data_width,_data_height,_data_depth);
}

QVector3D VolumeItem::voxelSize() const {
  return _voxel_size;
}

QVector3D VolumeItem::normalizedDataToVolumeScaling() const {
  return voxelSize()*(dataSize()-QVector3D(1.0,1.0,1.0));
}

QVector3D VolumeItem::centre() const {
  return 0.5*normalizedDataToVolumeScaling();
}

uint VolumeItem::textureID() const {
  return _texture_id;
}

void VolumeItem::setTextureID(uint i) {
  if (i!=_texture_id) {
    _texture_id=i;
    emit textureIDChanged(_texture_id);
  }
}

const QString& VolumeItem::description() const {
  return _description;
}

QQuickFramebufferObject::Renderer* VolumeItem::createRenderer() const {
  return new VolumeItemRenderer();
}

void VolumeItem::setDescription(const QString& msg) {
  if (msg!=_description) {
    _description=msg;
    emit descriptionChanged(description());
    std::cerr << "VolumeItem: " << description().toLocal8Bit().data() << std::endl;
  }
}

void VolumeItem::cleanup() {

  if (_data) {
    _preload.waitForFinished();
    _file->unmap(const_cast<uchar*>(_data));
  }
  _data=0;
  if (_file) {
    if (_file->isOpen()) {
      _file->close();
    }
    _file.reset();
  }
  _ok=false;
}
