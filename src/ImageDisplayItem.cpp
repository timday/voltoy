#include "ImageDisplayItem.h"

ImageDisplayItem::ImageDisplayItem(QQuickItem* parent) 
  :QQuickPaintedItem(parent)
{}

ImageDisplayItem::~ImageDisplayItem()
{}

const QImage& ImageDisplayItem::image() const {
  return _image;
}

void ImageDisplayItem::setImage(const QImage& i) {
  _image=i;
  emit imageChanged(image());
  update();
}

void ImageDisplayItem::paint(QPainter* painter) {
  if (!_image.isNull()) {
    painter->drawImage(0,0,_image);
  }
}
