#ifndef __voltoy_filetoucher_h__
#define __voltoy_filetoucher_h__

class Browser;

class FileToucher : public QObject {

Q_OBJECT

public:

  FileToucher(const QString& filename,Browser* parent);
  ~FileToucher();

  const QString& filename() const {return _filename;}
  QFutureWatcher<void>& watcher() {return *_futureWatcher;}

private:

  QString _filename;
  QFile _file;
  uchar* _data;
  QVector<const uchar*> _touch;
  QSharedPointer<QFutureWatcher<void> > _futureWatcher;
  QFuture<void> _future;
};

#endif
