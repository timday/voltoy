#include "ViewControlItem.h"

ViewControlItem::ViewControlItem(QQuickItem* parent)
  :QQuickItem(parent)
  ,_data_size(0.0,0.0,0.0)
  ,_voxel_size(0.0,0.0,0.0)
  ,_focus(0.0,0.0,0.0)
  ,_rotation(0.0)
  ,_tilt(0.0)
  ,_zoom(1.0)
{}

ViewControlItem::~ViewControlItem()
{}

const QVector3D& ViewControlItem::dataSize() const {
  return _data_size;
}

void ViewControlItem::setDataSize(const QVector3D& s) {
  if (s!=_data_size) {
    _data_size=s;
    emit dataSizeChanged(dataSize());
  }
}

const QVector3D& ViewControlItem::voxelSize() const {
  return _voxel_size;
}

void ViewControlItem::setVoxelSize(const QVector3D& s) {
  if (s!=_voxel_size) {
    _voxel_size=s;
    emit voxelSizeChanged(voxelSize());
  }
}

const QVector3D& ViewControlItem::focus() const {
  return _focus;
}

void ViewControlItem::setFocus(const QVector3D& p) {
  const QVector3D pt=clampedPoint(p);
  if (pt!=_focus && containsPoint(pt)) {
    _focus=pt;
    emit focusChanged(focus());
    recalculateTransforms();
  }
}

qreal ViewControlItem::rotation() const {
  return _rotation;
}

void ViewControlItem::setRotation(qreal r) {
  if (r!=_rotation) {
    _rotation=r;
    emit rotationChanged(rotation());
    recalculateTransforms();
  }
}

qreal ViewControlItem::tilt() const {
  return _tilt;
}

void ViewControlItem::setTilt(qreal t) {
  if (t!=_tilt) {
    _tilt=t;
    emit tiltChanged(tilt());
    recalculateTransforms();
  }
}

const QMatrix4x4& ViewControlItem::spin() const {
  return _spin;
}

qreal ViewControlItem::zoom() const {
  return _zoom;
}

void ViewControlItem::setZoom(qreal z) {
  if (z!=_zoom) {
    _zoom=z;
    emit zoomChanged(zoom());
    recalculateTransforms();
  }
} 

const QMatrix4x4& ViewControlItem::transformTransverse() const {
  return _transform_transverse;
}

const QMatrix4x4& ViewControlItem::transformCoronal() const {
  return _transform_coronal;
}

const QMatrix4x4& ViewControlItem::transformSagittal() const {
  return _transform_sagittal;
}

const QMatrix4x4& ViewControlItem::transformOblique() const {
  return _transform_oblique;
}

const QVector3D ViewControlItem::axisOblique() const {
  return QVector3D(_transform_oblique.column(2));
}

bool ViewControlItem::containsPoint(const QVector3D& pt) const {
  const QVector3D extent=voxelSize()*(dataSize()-QVector3D(1,1,1));
  return (
    pt.x()>=0.0
    &&
    pt.x()<=extent.x()
    &&
    pt.y()>=0.0
    &&
    pt.y()<=extent.y()
    &&
    pt.z()>=0.0
    &&
    pt.z()<=extent.z()
  );
}

void ViewControlItem::setSpin(const QMatrix4x4& s) {
  if (s!=_spin) {
    _spin=s;
    emit spinChanged(spin());
  }
}

void ViewControlItem::recalculateTransforms() {

  {
    QMatrix4x4 s;
    s.setColumn(0,QVector4D(1,0,0,0));
    s.setColumn(1,QVector4D(0,0,1,0));
    s.setColumn(2,QVector4D(0,1,0,0));
    s.rotate(rotation(),QVector3D(0.0,1.0,0.0)); 
    s.rotate(tilt(),QVector3D(1.0,0.0,0.0));
    setSpin(s);
  }

  // Transform for mapping screen coordinates over a [-1,1] viewport to the [0,1] volume data
  for (uint view=0;view<4;++view) {

    QMatrix4x4 axis_permute;
    // TODO: Some of these need negating; keyboard sense will need to be modified to match.
    switch (view) {
    case 0:
      {}
      break;
    case 1:
      {
	axis_permute.setColumn(0,QVector4D( 1,0,0,0));
	axis_permute.setColumn(1,QVector4D( 0,0,1,0));
	axis_permute.setColumn(2,QVector4D( 0,1,0,0));
      }
      break;
    case 2:
      {
	axis_permute.setColumn(0,QVector4D( 0,1,0,0));
	axis_permute.setColumn(1,QVector4D( 0,0,1,0));
	axis_permute.setColumn(2,QVector4D( 1,0,0,0));
      }
      break;
    case 3:
      {
	axis_permute=spin();
      }
      break;
    }

    QMatrix4x4 transform_zoom;
    transform_zoom.scale(1.0/_zoom);
    
    QMatrix4x4 transform_scale_to_data;
    transform_scale_to_data.scale(inv(_voxel_size));
    
    const qreal max_dim=maxDim(_data_size-QVector3D(1.0,1.0,1.0));
    QMatrix4x4 data_aspect;
    data_aspect.scale(QVector3D(max_dim,max_dim,max_dim)*inv(_data_size-QVector3D(1.0,1.0,1.0)));

    const QVector3D focus_in_data(_focus*inv(_voxel_size*(_data_size-QVector3D(1.0,1.0,1.0))));
    QMatrix4x4 transform_pan;
    transform_pan.translate(focus_in_data);
    
    const QMatrix4x4 result=transform_pan*transform_scale_to_data*data_aspect*transform_zoom*axis_permute;
    
    switch (view) {
    case 0:
      if (result!=_transform_transverse) {
	_transform_transverse=result;
	emit transformTransverseChanged(transformTransverse());
      }
      break;
    case 1:
      if (result!=_transform_coronal) {
	_transform_coronal=result;
	emit transformCoronalChanged(transformCoronal());
      }
      break;
    case 2:
      if (result!=_transform_sagittal) {
	_transform_sagittal=result;
	emit transformSagittalChanged(transformSagittal());
      }
      break;
    case 3:
      if (result!=_transform_oblique) {
	_transform_oblique=result;
	emit transformObliqueChanged(transformOblique());
	emit axisObliqueChanged(axisOblique());
      }
      break;
    }
  }
}

QVector3D ViewControlItem::clampedPoint(const QVector3D& pt) const {
  const QVector3D extent=voxelSize()*(dataSize()-QVector3D(1,1,1));
  return QVector3D(
    std::max(0.0f,std::min(pt.x(),extent.x())),
    std::max(0.0f,std::min(pt.y(),extent.y())),
    std::max(0.0f,std::min(pt.z(),extent.z()))
  );
}
