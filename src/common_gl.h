#ifndef __voltoy_common_gl_h__
#define __voltoy_common_gl_h__

// QOpenGLFunctions is OpenGLES oriented and doesn't include 3D textures.
//#include <QOpenGLFunctions>

#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions_2_1>
#include <QOpenGLShaderProgram>

typedef QOpenGLFunctions_2_1 GLFns;

extern void glerrcheck(GLFns& gl,const char* where);

#endif
