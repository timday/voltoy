#ifndef __voltoy_mpritem_h__
#define __voltoy_mpritem_h__

#include "MPRItemBase.h"

class MPRItem : public MPRItemBase {

  Q_OBJECT
  Q_ENUMS(Plane)
  Q_PROPERTY(Plane plane READ plane WRITE setPlane NOTIFY planeChanged)
  Q_PROPERTY(QColor outlineColor READ outlineColor WRITE setOutlineColor NOTIFY outlineColorChanged)

public:

  enum Plane {
    Transverse,Sagittal,Coronal,Oblique
  };

  MPRItem(QQuickItem* parent=0);
  ~MPRItem();

  Plane plane() const;
  void setPlane(Plane);

  const QColor& outlineColor() const;
  void setOutlineColor(const QColor&);

  QQuickFramebufferObject::Renderer* createRenderer() const;

signals:

  void planeChanged(Plane);
  void outlineColorChanged(const QColor&);

private:

  Plane _plane;
  QColor _outline_color;
};

#endif
