#include "MPRsItem.h"

#include "MPRsItemRenderer.h"

MPRsItem::MPRsItem(QQuickItem* parent)
  :MPRItemBase(parent)
{}

MPRsItem::~MPRsItem()
{}

QQuickFramebufferObject::Renderer* MPRsItem::createRenderer() const {
  return new MPRsItemRenderer();
};
