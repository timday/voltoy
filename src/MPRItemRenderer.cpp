#include "MPRItemRenderer.h"

#include "MPRItem.h"

MPRItemRenderer::MPRItemRenderer()
{
  {
    const bool vshaderok=_program_mpr.addShaderFromSourceCode(
      QOpenGLShader::Vertex,
      "attribute highp vec4 vertex_coord;\n"
      "uniform highp mat4 transform;\n"
      "varying highp vec3 v_texture_coord;\n"
      "void main(void)\n"
      "{\n"
      "  gl_Position = vertex_coord;\n"
      "  v_texture_coord = vec3(transform*vertex_coord);\n"
      "}\n"
    );
    Q_ASSERT(vshaderok);
    
    const bool fshaderok=_program_mpr.addShaderFromSourceCode(
      QOpenGLShader::Fragment,
      "uniform sampler3D texture;\n"
      "uniform highp float wlMin;\n"
      "uniform highp float wlScale;\n"      
      "varying highp vec3 v_texture_coord;\n"
      "void main(void)\n"
      "{\n"
          "if (v_texture_coord.x<0.0 || v_texture_coord.x>1.0 || v_texture_coord.y<0.0 || v_texture_coord.y>1.0 || v_texture_coord.z<0.0 || v_texture_coord.z>1.0) {\n"
          "  gl_FragColor = vec4(0.0,0.0,0.0,0.0);\n"
          "} else {\n"
          "  highp float v=texture3D(texture,v_texture_coord).r;\n"
          "  highp float s=max(0.0,min(wlScale*(v-wlMin),1.0));\n"
          "  gl_FragColor = vec4(s,s,s,s);\n"
          "}\n"
      "}\n"
    );
    Q_ASSERT(fshaderok);
    
    const bool linkok=_program_mpr.link();
    Q_ASSERT(linkok);
  }
  
  {
    const bool vshaderok=_program_outline.addShaderFromSourceCode(
      QOpenGLShader::Vertex,
      "attribute highp vec4 vertex_coord;\n"
      "uniform highp mat4 transform;\n"
      "void main(void)\n"
      "{\n"
      "  gl_Position = transform*vertex_coord;\n"
      "}\n"
    );
    Q_ASSERT(vshaderok);
    
    const bool fshaderok=_program_outline.addShaderFromSourceCode(
      QOpenGLShader::Fragment,
      "uniform highp vec4 color;\n"
      "void main(void)\n"
      "{\n"
          "gl_FragColor = color;\n"
      "}\n"
    );
    Q_ASSERT(fshaderok);
    
    const bool linkok=_program_outline.link();
    Q_ASSERT(linkok);    
  }

  glerrcheck(_gl,"MPRItemRenderer ctor");
}

MPRItemRenderer::~MPRItemRenderer()
{}

void MPRItemRenderer::synchronize(QQuickFramebufferObject* item) {
  MPRItemRendererBase::synchronize(item);

  const MPRItem*const mpr=dynamic_cast<const MPRItem*>(item);
  if (!mpr) {
    std::cerr << "Unexpected argument to MPRItemRenderer::synchronize" << std::endl;
  }

  _plane=mpr->plane();
  _outline_color=mpr->outlineColor();
}

void MPRItemRenderer::render()
{
  glerrcheck(_gl,"MPRItemRenderer::render start");

  const QColor colour(0,0,0,0);
  _gl.glClearColor(colour.redF(),colour.greenF(),colour.blueF(),colour.alphaF());
  _gl.glClear(GL_COLOR_BUFFER_BIT);

  render_mpr();
  render_outline();

  glerrcheck(_gl,"MPRItemRenderer::render end");
  std::cerr << "*";
}

void MPRItemRenderer::render_mpr()
{
  std::vector<QVector3D> geometry_mpr;
  geometry_mpr.push_back(QVector3D(-1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f, 1.0f, 0.0f));
  
  geometry_mpr.push_back(QVector3D(-1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f, 1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D(-1.0f, 1.0f, 0.0f));
  
  if (!_program_mpr.bind()) {
    std::cerr << "[Error binding shader program]";
  }

  _gl.glEnable(GL_TEXTURE_3D);
  _gl.glDisable(GL_DEPTH_TEST);
  _gl.glDepthMask(GL_FALSE);

  _gl.glBindTexture(GL_TEXTURE_3D,_texture_id);

  QMatrix4x4 actual_transform;
  {
    QMatrix4x4 aspect;
    aspect.scale(std::max(1.0,_width/_height),std::max(1.0,_height/_width),1.0f);
    actual_transform=_transform*aspect;
  }

  const int transform_location=_program_mpr.uniformLocation("transform");
  Q_ASSERT(transform_location!=-1);
  _program_mpr.setUniformValue(transform_location,actual_transform);

  const int vertexCoord_location=_program_mpr.attributeLocation("vertex_coord");
  Q_ASSERT(vertexCoord_location!=-1);
  _program_mpr.enableAttributeArray(vertexCoord_location);
  _program_mpr.setAttributeArray(vertexCoord_location,&geometry_mpr[0]);

  const int wlMin_location=_program_mpr.uniformLocation("wlMin");
  Q_ASSERT(wlMin_location!=-1);
  _program_mpr.setUniformValue(wlMin_location,float((_wlLevel-0.5*_wlWindow)/65535.0));
  
  const int wlScale_location=_program_mpr.uniformLocation("wlScale");
  Q_ASSERT(wlScale_location!=-1);
  _program_mpr.setUniformValue(wlScale_location,float(1.0/std::max(qreal(1e-6),_wlWindow/65535.0)));
  
  _gl.glActiveTexture(GL_TEXTURE0);
  const int texture_location=_program_mpr.uniformLocation("texture");
  _program_mpr.setUniformValue(texture_location,0);
  
  _gl.glDrawArrays(GL_TRIANGLES,0,geometry_mpr.size());

  _program_mpr.disableAttributeArray(vertexCoord_location);

  _gl.glEnable(GL_TEXTURE_3D);

  _gl.glBindTexture(GL_TEXTURE_3D,0);

  _program_mpr.release();
}

void MPRItemRenderer::render_outline() {

  const QVector3D pt=_transform*QVector3D(0.0,0.0,0.0);

  std::vector<QVector3D> geometry_outline;  

  switch (_plane) {
  case MPRItem::Transverse:
    geometry_outline.push_back(QVector3D(0.0f,0.0f,pt.z()));
    geometry_outline.push_back(QVector3D(1.0f,0.0f,pt.z()));
    geometry_outline.push_back(QVector3D(1.0f,1.0f,pt.z()));
    geometry_outline.push_back(QVector3D(0.0f,1.0f,pt.z()));
    geometry_outline.push_back(QVector3D(0.0f,0.0f,pt.z()));
    break;
  case MPRItem::Coronal:
    geometry_outline.push_back(QVector3D(0.0f,pt.y(),0.0f));
    geometry_outline.push_back(QVector3D(1.0f,pt.y(),0.0f));
    geometry_outline.push_back(QVector3D(1.0f,pt.y(),1.0f));
    geometry_outline.push_back(QVector3D(0.0f,pt.y(),1.0f));
    geometry_outline.push_back(QVector3D(0.0f,pt.y(),0.0f));    
    break;
  case MPRItem::Sagittal:
    geometry_outline.push_back(QVector3D(pt.x(),0.0f,0.0f));
    geometry_outline.push_back(QVector3D(pt.x(),1.0f,0.0f));
    geometry_outline.push_back(QVector3D(pt.x(),1.0f,1.0f));
    geometry_outline.push_back(QVector3D(pt.x(),0.0f,1.0f));
    geometry_outline.push_back(QVector3D(pt.x(),0.0f,0.0f));    
    break;
  case MPRItem::Oblique:
    return;
  }

  if (!_program_outline.bind()) {
    std::cerr << "[Error binding shader program]";
  }

  _gl.glDisable(GL_DEPTH_TEST);
  _gl.glDepthMask(GL_FALSE);

  QMatrix4x4 actual_transform;
  {
    QMatrix4x4 aspect;
    aspect.scale(std::max(1.0,_width/_height),std::max(1.0,_height/_width),1.0f);

    QMatrix4x4 collapse_z;
    collapse_z.setColumn(2,QVector4D(0.0,0.0,0.0,0.0));
    collapse_z.setColumn(3,QVector4D(0.0,0.0,1.0,1.0));

    actual_transform=collapse_z*(_transform*aspect).inverted();
  }

  const int transform_location=_program_outline.uniformLocation("transform");
  Q_ASSERT(transform_location!=-1);
  _program_mpr.setUniformValue(transform_location,actual_transform);

  const int vertexCoord_location=_program_outline.attributeLocation("vertex_coord");
  Q_ASSERT(vertexCoord_location!=-1);
  _program_outline.enableAttributeArray(vertexCoord_location);
  _program_outline.setAttributeArray(vertexCoord_location,&geometry_outline[0]);

  const int color_location=_program_outline.uniformLocation("color");
  Q_ASSERT(color_location!=-1);
  _program_outline.setUniformValue(color_location,_outline_color);
  
  _gl.glDrawArrays(GL_LINE_STRIP,0,geometry_outline.size());

  _program_outline.disableAttributeArray(vertexCoord_location);

  _program_outline.release();
}
