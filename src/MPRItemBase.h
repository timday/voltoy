#ifndef __voltoy_mpritembase_h__
#define __voltoy_mpritembase_h__

class MPRItemBase : public QQuickFramebufferObject {

  Q_OBJECT

  Q_PROPERTY(uint textureID READ textureID WRITE setTextureID NOTIFY textureIDChanged)
  Q_PROPERTY(QMatrix4x4 transform READ transform WRITE setTransform NOTIFY transformChanged)
  Q_PROPERTY(qreal wlWindow READ wlWindow WRITE setWlWindow NOTIFY wlWindowChanged)
  Q_PROPERTY(qreal wlLevel READ wlLevel WRITE setWlLevel NOTIFY wlLevelChanged)

public:

  MPRItemBase(QQuickItem* parent=0);
  ~MPRItemBase();

  uint textureID() const;
  void setTextureID(uint);

  const QMatrix4x4& transform() const;
  void setTransform(const QMatrix4x4&);

  qreal wlWindow() const;
  void setWlWindow(qreal);

  qreal wlLevel() const;
  void setWlLevel(qreal);

  void synced() const;

signals:

  void textureIDChanged(uint);
  void transformChanged(const QMatrix4x4&);
  void wlWindowChanged(qreal);
  void wlLevelChanged(qreal);
  void frameEvent() const;

private:

  uint _texture_id;
  QMatrix4x4 _transform;
  qreal _wlWindow;
  qreal _wlLevel;
};

#endif
