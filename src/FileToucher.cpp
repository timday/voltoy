#include "FileToucher.h"

#include "Browser.h"
#include "force.h"

namespace {

  const qint64 pageSize=4096;

  void touch(const uchar* ptr) {
    force(*ptr);
  }

  void noop() {}

}

FileToucher::FileToucher(const QString& filename,Browser* browser)
  :QObject(browser)
  ,_filename(filename)
  ,_file(filename)
  ,_data(0)
  ,_futureWatcher(new QFutureWatcher<void>(this))
{
  std::cerr << "FileToucher: touching " << filename.toLocal8Bit().data() << std::endl;

  if (!QObject::connect(_futureWatcher.data(),SIGNAL(finished()),browser,SLOT(touchFileFinished()))) {
    std::cerr << "FileToucher: couldn't connect QFutureWatcher to Browser" << std::endl;
  }

  if (_file.open(QIODevice::ReadOnly)) {
    _data=_file.map(0,_file.size());
    if (_data) {
      _touch.reserve(_file.size()/pageSize);
      for (qint64 i=0;i<_file.size();i+=pageSize) {
	_touch.push_back(_data+i);
      }
      _future=QtConcurrent::map(_touch,touch);
    } else {
      std::cerr << "FileToucher: failed to map " << filename.toLocal8Bit().data() << std::endl;
      _future=QtConcurrent::run(noop);
    }
  } else {
    std::cerr << "FileToucher: failed to open " << filename.toLocal8Bit().data() << std::endl;      
    _future=QtConcurrent::run(noop);
  }

  _futureWatcher->setFuture(_future);
}

FileToucher::~FileToucher() {
  _futureWatcher->cancel();
  _futureWatcher->waitForFinished();
  if (_data) {
    _file.unmap(_data);
  }
}
