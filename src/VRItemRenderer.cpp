#include "VRItemRenderer.h"

#include "VRItem.h"

VRItemRenderer::VRItemRenderer()
  :_normalizedDataToVolumeScaling(0.0,0.0,0.0)
{
  {
    const bool vshaderok=_program_vr.addShaderFromSourceCode(
      QOpenGLShader::Vertex,
      "attribute highp vec4 vertex_coord;\n"
      "uniform highp mat4 viewTransform;\n"
      "varying highp vec3 viewPosition;\n"
      "void main(void)\n"
      "{\n"
      "  gl_Position = vertex_coord;\n"
      "  viewPosition = vec3(viewTransform*vertex_coord);\n"
      "}\n"
    );
    Q_ASSERT(vshaderok);
    
    const bool fshaderok=_program_vr.addShaderFromSourceCode(
      QOpenGLShader::Fragment,
      "uniform sampler3D texture;\n"
      "uniform highp mat4 volumeTransform;\n"
      "uniform highp float wlMin;\n"
      "uniform highp float wlScale;\n"
      "uniform highp vec3 invstep;\n"  
      "varying highp vec3 viewPosition;\n"
      "\n"
      "void main(void)\n"
      "{\n"
      "  highp vec3 p=vec3(volumeTransform*vec4(viewPosition,1.0));\n"
      "  highp vec3 r=normalize(vec3(volumeTransform*vec4(viewPosition+vec3(0.0,0.0,1.0),1.0))-p);\n"
      "  highp vec3 d0=(vec3(0.0,0.0,0.0)-p)/r;\n"    // Distance to a '0' face of unit cube 
      "  highp vec3 d1=(vec3(1.0,1.0,1.0)-p)/r;\n"    // Distance to a '1' face of unit cube 
      "  highp float xlo=(r.x==0.0 ? -128.0 : (r.x>0.0 ? d0.x : d1.x));\n"
      "  highp float xhi=(r.x==0.0 ?  128.0 : (r.x>0.0 ? d1.x : d0.x));\n"
      "  highp float ylo=(r.y==0.0 ? -128.0 : (r.y>0.0 ? d0.y : d1.y));\n"
      "  highp float yhi=(r.y==0.0 ?  128.0 : (r.y>0.0 ? d1.y : d0.y));\n"
      "  highp float zlo=(r.z==0.0 ? -128.0 : (r.z>0.0 ? d0.z : d1.z));\n"
      "  highp float zhi=(r.z==0.0 ?  128.0 : (r.z>0.0 ? d1.z : d0.z));\n"
      "  highp float lo=max(zlo,max(ylo,xlo));\n"
      "  highp float hi=min(zhi,min(yhi,xhi));\n"
      "  highp float maxv=-65536.0;\n"
      "  highp float step=0.5*dot(invstep,abs(r));\n"    // Conservative stepping
      "  for (float l=lo;l<hi;l+=step) {\n"
      "    highp float v=texture3D(texture,p+l*r).r;\n"
      "    if (v>maxv) maxv=v;\n"
      "  }\n"
      "  highp float s=max(0.0,min(wlScale*(maxv-wlMin),1.0));\n"
      "  gl_FragColor = vec4(s,s,s,s);\n"
      "}\n"
    );
    Q_ASSERT(fshaderok);
    
    const bool linkok=_program_vr.link();
    Q_ASSERT(linkok);
  }
  
  glerrcheck(_gl,"VRItemRenderer ctor");
}

VRItemRenderer::~VRItemRenderer()
{}

void VRItemRenderer::synchronize(QQuickFramebufferObject* item) {
  MPRItemRendererBase::synchronize(item);

  VRItem*const vr_item=dynamic_cast<VRItem*>(item);
  if (!vr_item) {
    std::cerr << "Unexpected argument to VRItemRenderer::synchronize" << std::endl;
  }

  _normalizedDataToVolumeScaling=vr_item->normalizedDataToVolumeScaling();
}

void VRItemRenderer::render()
{
  glerrcheck(_gl,"VRItemRenderer::render start");

  const QColor colour(0,0,0,0);
  _gl.glClearColor(colour.redF(),colour.greenF(),colour.blueF(),colour.alphaF());
  _gl.glClear(GL_COLOR_BUFFER_BIT);

  std::vector<QVector3D> geometry_mpr;
  geometry_mpr.push_back(QVector3D(-1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f, 1.0f, 0.0f));
  
  geometry_mpr.push_back(QVector3D(-1.0f,-1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D( 1.0f, 1.0f, 0.0f));
  geometry_mpr.push_back(QVector3D(-1.0f, 1.0f, 0.0f));
  
  if (!_program_vr.bind()) {
    std::cerr << "[Error binding shader program]";
  }

  _gl.glEnable(GL_TEXTURE_3D);
  _gl.glDisable(GL_DEPTH_TEST);
  _gl.glDepthMask(GL_FALSE);

  _gl.glBindTexture(GL_TEXTURE_3D,_texture_id);

  QMatrix4x4 view_transform;
  {
    QMatrix4x4 aspect;
    aspect.scale(std::max(1.0,_width/_height),std::max(1.0,_height/_width),1.0f);
    view_transform=aspect;
  }

  QMatrix4x4 volume_transform;
  {
    volume_transform=_transform;
  }

  const int view_transform_location=_program_vr.uniformLocation("viewTransform");
  Q_ASSERT(view_transform_location!=-1);
  _program_vr.setUniformValue(view_transform_location,view_transform);

  const int volume_transform_location=_program_vr.uniformLocation("volumeTransform");
  Q_ASSERT(volume_transform_location!=-1);
  _program_vr.setUniformValue(volume_transform_location,volume_transform);

  const int vertexCoord_location=_program_vr.attributeLocation("vertex_coord");
  Q_ASSERT(vertexCoord_location!=-1);
  _program_vr.enableAttributeArray(vertexCoord_location);
  _program_vr.setAttributeArray(vertexCoord_location,&geometry_mpr[0]);

  const int wlMin_location=_program_vr.uniformLocation("wlMin");
  Q_ASSERT(wlMin_location!=-1);
  _program_vr.setUniformValue(wlMin_location,float((_wlLevel-0.5*_wlWindow)/65535.0));
  
  const int wlScale_location=_program_vr.uniformLocation("wlScale");
  Q_ASSERT(wlScale_location!=-1);
  _program_vr.setUniformValue(wlScale_location,float(1.0/std::max(qreal(1e-6),_wlWindow/65535.0)));

  const int invstep_location=_program_vr.uniformLocation("invstep");
  Q_ASSERT(invstep_location!=-1);
  _program_vr.setUniformValue(invstep_location,inv(_normalizedDataToVolumeScaling));
  
  _gl.glActiveTexture(GL_TEXTURE0);
  const int texture_location=_program_vr.uniformLocation("texture");
  _program_vr.setUniformValue(texture_location,0);
  
  _gl.glDrawArrays(GL_TRIANGLES,0,geometry_mpr.size());

  _program_vr.disableAttributeArray(vertexCoord_location);

  _gl.glBindTexture(GL_TEXTURE_3D,0);

  _gl.glDisable(GL_TEXTURE_3D);

  _program_vr.release();

  glerrcheck(_gl,"VRItemRenderer::render end");
  std::cerr << "[VR]";
}
