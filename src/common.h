#ifndef __voltoy_common_h__
#define __voltoy_common_h__

#include <array>
#include <deque>
#include <iomanip>
#include <iostream>
#include <vector>

// See also common_gl.h

#include <QColor>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QFuture>
#include <QFutureWatcher>
#include <QGuiApplication>
#include <QHash>
#include <QMap>
#include <QMatrix4x4>
#include <QPainter>
#include <QQmlContext>
#include <QQuickFramebufferObject>
#include <QStandardPaths>
#include <QThread>
#include <QQuickItem>
#include <QQuickPaintedItem>
#include <QScopedPointer>
#include <QtConcurrent>
#include <QTimer>
#include <QtGlobal>
#include <QUrl>
#include <QUrlQuery>
#include <QVector>
#include <QVector3D>
#include <QVector4D>
#include <QQuickView>

inline QVector3D inv(const QVector3D& v) {
  return QVector3D(1.0/v.x(),1.0/v.y(),1.0/v.z());
}

extern qreal minDim(const QVector3D&);
extern qreal maxDim(const QVector3D&);

std::ostream& operator<<(std::ostream&,const QVector3D&);
std::ostream& operator<<(std::ostream&,const QVector4D&);
std::ostream& operator<<(std::ostream&,const QMatrix4x4&);

#endif
