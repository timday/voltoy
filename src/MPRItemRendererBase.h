#ifndef __voltoy_mpritemrendererbase_h__
#define __voltoy_mpritemrendererbase_h__

class MPRItemRendererBase : public QQuickFramebufferObject::Renderer {

public:

  MPRItemRendererBase();
  ~MPRItemRendererBase();

  void synchronize(QQuickFramebufferObject*);

protected:

  GLFns _gl;
  qreal _width;
  qreal _height;
  uint _texture_id;
  QMatrix4x4 _transform;
  qreal _wlWindow;
  qreal _wlLevel;
};

#endif
