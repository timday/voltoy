#include "MPRItemBase.h"

MPRItemBase::MPRItemBase(QQuickItem* parent)
  :QQuickFramebufferObject(parent)
  ,_texture_id(0)
  ,_wlWindow(1024.0)
  ,_wlLevel(1024.0)
{}

MPRItemBase::~MPRItemBase()
{}

uint MPRItemBase::textureID() const {
  return _texture_id;
}

void MPRItemBase::setTextureID(uint i) {
  if (i!=_texture_id) {
    _texture_id=i;
    emit textureIDChanged(_texture_id);
    update();
  }
}

const QMatrix4x4& MPRItemBase::transform() const {
  return _transform;
}

void MPRItemBase::setTransform(const QMatrix4x4& t) {
  if (!qFuzzyCompare(t,_transform)) {
    _transform=t;
    emit transformChanged(transform());
    update();
  }
} 

qreal MPRItemBase::wlWindow() const {
  return _wlWindow;
}

void MPRItemBase::setWlWindow(qreal v) {
  if (v!=_wlWindow) {
    _wlWindow=v;
    emit wlWindowChanged(wlWindow());
    update();
  }
}

qreal MPRItemBase::wlLevel() const {
  return _wlLevel;
}

void MPRItemBase::setWlLevel(qreal v) {
  if (v!=_wlLevel) {
    _wlLevel=v;
    emit wlLevelChanged(wlLevel());
    update();
  }
}

void MPRItemBase::synced() const {
  emit frameEvent();
}
