#ifndef __voltoy_viewcontrolitem_h__
#define __voltoy_viewcontrolitem_h__

class ViewControlItem : public QQuickItem {

  Q_OBJECT

  Q_ENUMS(MouseDragMode) // Not used here; just provided for convenience in QML code.

  Q_PROPERTY(QVector3D dataSize READ dataSize WRITE setDataSize NOTIFY dataSizeChanged)
  Q_PROPERTY(QVector3D voxelSize READ voxelSize WRITE setVoxelSize NOTIFY voxelSizeChanged)

  Q_PROPERTY(QVector3D focus READ focus WRITE setFocus NOTIFY focusChanged)
  Q_PROPERTY(qreal rotation READ rotation WRITE setRotation NOTIFY rotationChanged)
  Q_PROPERTY(qreal tilt READ tilt WRITE setTilt NOTIFY tiltChanged)
  Q_PROPERTY(QMatrix4x4 spin READ spin NOTIFY spinChanged)  // Derived from rotation and tilt
  Q_PROPERTY(qreal zoom READ zoom WRITE setZoom NOTIFY zoomChanged)

  Q_PROPERTY(QMatrix4x4 transformTransverse READ transformTransverse NOTIFY transformTransverseChanged)
  Q_PROPERTY(QMatrix4x4 transformCoronal READ transformCoronal NOTIFY transformCoronalChanged)
  Q_PROPERTY(QMatrix4x4 transformSagittal READ transformSagittal NOTIFY transformSagittalChanged)
  Q_PROPERTY(QMatrix4x4 transformOblique READ transformOblique NOTIFY transformObliqueChanged)

  Q_PROPERTY(QVector3D axisOblique READ axisOblique NOTIFY axisObliqueChanged)
  
public:

  enum MouseDragMode{
    MouseDragModePan,
    MouseDragModeRotate,
    MouseDragModeZoom,
    MouseDragModeCine,
    MouseDragModeWindowLevel
  };

  ViewControlItem(QQuickItem* parent=0);
  ~ViewControlItem();

  const QVector3D& dataSize() const;
  void setDataSize(const QVector3D&);

  const QVector3D& voxelSize() const;
  void setVoxelSize(const QVector3D&);

  const QVector3D& focus() const;
  void setFocus(const QVector3D&);

  qreal rotation() const;
  void setRotation(qreal);

  qreal tilt() const;
  void setTilt(qreal);

  const QMatrix4x4& spin() const;
  
  qreal zoom() const;
  void setZoom(qreal);

  // These are the transforms to map coordinates in a normalized [-1,1] viewport
  // to the specified plane through the focal point in the normalized [0,1] data.
  const QMatrix4x4& transformTransverse() const;
  const QMatrix4x4& transformCoronal() const;
  const QMatrix4x4& transformSagittal() const;
  const QMatrix4x4& transformOblique() const;
  const QVector3D axisOblique() const;

  Q_INVOKABLE bool containsPoint(const QVector3D&) const;

signals:

  void dataSizeChanged(const QVector3D&);
  void voxelSizeChanged(const QVector3D&);
  void focusChanged(const QVector3D&);
  void rotationChanged(qreal);
  void tiltChanged(qreal);
  void spinChanged(const QMatrix4x4&);
  void zoomChanged(qreal);
  void transformTransverseChanged(const QMatrix4x4&);
  void transformCoronalChanged(const QMatrix4x4&);
  void transformSagittalChanged(const QMatrix4x4&);
  void transformObliqueChanged(const QMatrix4x4&);
  void axisObliqueChanged(QVector3D);
  void cineChanged(const QVector3D&);
  void cineingChanged(bool);

private:

  void setSpin(const QMatrix4x4&);
  
  void recalculateTransforms();

  QVector3D clampedPoint(const QVector3D&) const;

  QVector3D _data_size;
  QVector3D _voxel_size;
  QVector3D _focus;
  qreal _rotation;
  qreal _tilt;
  QMatrix4x4 _spin;
  qreal _zoom;
  QMatrix4x4 _transform_transverse;
  QMatrix4x4 _transform_coronal;
  QMatrix4x4 _transform_sagittal;
  QMatrix4x4 _transform_oblique;
};

#endif
