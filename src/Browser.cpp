#include "Browser.h"

#include <QQmlContext>

Browser::Browser() {

  setSurfaceType(QSurface::OpenGLSurface);

  QSurfaceFormat format;
#ifdef IOSQUIRKS
  format.setRenderableType(QSurfaceFormat::OpenGLES);
#else
  format.setRenderableType(QSurfaceFormat::OpenGL);
#endif
  format.setAlphaBufferSize(8);
  format.setStencilBufferSize(8);
  setFormat(format);

  setFlags(flags() | Qt::WindowFullscreenButtonHint);

  rootContext()->setContextProperty("browser",this);

  QObject::connect(&_animationTicker,SIGNAL(timeout()),this,SIGNAL(animationTick()));
  _animationTicker.setTimerType(Qt::PreciseTimer);
  _animationTicker.setSingleShot(false);
  _animationTicker.start(1000.0/30.0);
}

Browser::~Browser() {
}

QString Browser::fileContents(const QString& filename) const {
  QFile file(filename);
  if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QTextStream in(&file);
    return in.readAll();
  } else {
    return QString();
  }
}

void Browser::touchFile(const QString& filename) {
  QSharedPointer<FileToucher> toucher(new FileToucher(filename,this));
  _touchJobs.push_back(toucher);
}

void Browser::cancelAllTouchFiles() {
  uint canceled=0;
  for (int i=0;i<_touchJobs.size();++i) {
    if (!_touchJobs[i]->watcher().isCanceled()) {
      _touchJobs[i]->watcher().cancel();
      ++canceled;
    }
  }
  if (canceled) {
    std::cerr << "Browser::cancelAllTouchFiles: canceled " << canceled << " jobs" << std::endl;
  }
}

QString Browser::parameter(const QString& p) const {
  const QHash<QString,QString>::const_iterator it=_url_parameters_lookup.find(p);
  if (it==_url_parameters_lookup.end()) {
    return QString();
  } else {
    return *it;
  }
}

void Browser::captureScreenshot() {
  setCapture(grabWindow());
}

const QImage& Browser::capture() const {
  return _capture;
}

void Browser::setCapture(const QImage& i) {
  std::cerr << "Browser::setCapture: " << i.width() << "x" << i.height() << " image" << std::endl;
  _capture=i;
  emit captureChanged(capture());
}

QStringList Browser::options() const {
  return QCoreApplication::arguments();
}

void Browser::fullscreen() {
  if (windowState()&Qt::WindowFullScreen) {
    showMaximized();
  } else {
    showFullScreen();
  }
}

void Browser::openInExternalBrowser(const QUrl& url) {
  QDesktopServices::openUrl(url);
}

void Browser::touchFileFinished() {
  for (int i=0;i<_touchJobs.size();++i) {
    if (_touchJobs[i]->watcher().isFinished()) {
      if (!_touchJobs[i]->watcher().isCanceled()) {
	emit touchedFile(_touchJobs[i]->filename());
      }
      _touchJobs.removeAt(i);
      touchFileFinished();
      return;
    }
  }
} 

void Browser::load(const QUrl& url) {
  // NB must make async call to update page content to avoid crash as calling QML page is torn down
  // https://bugreports.qt-project.org/browse/QTBUG-19410
  QMetaObject::invokeMethod(this,"loadImmediate",Qt::QueuedConnection,Q_ARG(QUrl,url));
}

void Browser::loadImmediate(const QUrl& url) {
  std::cerr << "Loading " << url.toString().toLocal8Bit().data() << std::endl;
  if (url.hasQuery()) {
    QUrlQuery query(url);
    setParameters(query.queryItems());
  }
  _url=url.adjusted(QUrl::RemoveQuery|QUrl::RemoveFragment);
  setSource(_url);
  // TODO: Should check status() here!
}

void Browser::setParameters(const QList<QPair<QString, QString> >& p) {
  _url_parameters=p;
  _url_parameters_lookup.clear();
  for (int i=0;i<_url_parameters.size();++i) {
    _url_parameters_lookup.insert(_url_parameters[i].first,_url_parameters[i].second);
  }
}
