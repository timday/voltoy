#include "MPRItem.h"

#include "MPRItemRenderer.h"

MPRItem::MPRItem(QQuickItem* parent)
  :MPRItemBase(parent)
  ,_plane(Transverse)
  ,_outline_color(255,0,0)
{}

MPRItem::~MPRItem()
{}

MPRItem::Plane MPRItem::plane() const {
  return _plane;
}

void MPRItem::setPlane(MPRItem::Plane p) {
  if (p!=_plane) {
    _plane=p;
    emit planeChanged(plane());
    update();
  }
}

const QColor& MPRItem::outlineColor() const {
  return _outline_color;
}

void MPRItem::setOutlineColor(const QColor& c) {
  if (c!=_outline_color) {
    _outline_color=c;
    emit outlineColorChanged(outlineColor());
    update();
  }
}

QQuickFramebufferObject::Renderer* MPRItem::createRenderer() const {
  return new MPRItemRenderer();
};
