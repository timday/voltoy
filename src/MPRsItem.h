#ifndef __voltoy_mprsitem_h__
#define __voltoy_mprsitem_h__

#include "MPRItemBase.h"

class MPRsItem : public MPRItemBase {

  Q_OBJECT

public:

  MPRsItem(QQuickItem* parent=0);
  ~MPRsItem();

  QQuickFramebufferObject::Renderer* createRenderer() const;
};

#endif
