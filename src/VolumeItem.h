#ifndef __voltoy_volumeitem_h__
#define __voltoy_volumeitem_h__

class VolumeItem : public QQuickFramebufferObject {

  Q_OBJECT

  Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
  Q_PROPERTY(QVector3D dataSize READ dataSize NOTIFY dataSizeChanged)
  Q_PROPERTY(QVector3D voxelSize READ voxelSize NOTIFY voxelSizeChanged)
  Q_PROPERTY(QVector3D normalizedDataToVolumeScaling READ normalizedDataToVolumeScaling NOTIFY normalizedDataToVolumeScalingChanged) 
  Q_PROPERTY(QVector3D centre READ centre NOTIFY centreChanged)

  Q_PROPERTY(uint textureID READ textureID WRITE setTextureID NOTIFY textureIDChanged)
  Q_PROPERTY(QString description READ description NOTIFY descriptionChanged)

public:
  
  VolumeItem(QQuickItem* parent=0);
  ~VolumeItem();

  QUrl source() const;
  void setSource(const QUrl&);

  bool changedForRenderer() const;
  void setChangedForRenderer(bool);

  const uchar* data() const;
  uint dataWidth() const;
  uint dataHeight() const;
  uint dataDepth() const;

  QVector3D dataSize() const;
  QVector3D voxelSize() const;
  QVector3D normalizedDataToVolumeScaling() const;
  QVector3D centre() const;

  uint textureID() const;
  void setTextureID(uint);

  const QString& description() const;

  QQuickFramebufferObject::Renderer* createRenderer() const;

signals:

  void sourceChanged(const QUrl&);
  void textureIDChanged(uint);
  void dataSizeChanged(QVector3D);
  void voxelSizeChanged(QVector3D);
  void normalizedDataToVolumeScalingChanged(QVector3D);
  void centreChanged(QVector3D);
  void descriptionChanged(const QString&);

private:

  void setDescription(const QString&);

  void cleanup();

  bool _ok;
  bool _changed_for_renderer;
  QUrl _source;
  QScopedPointer<QFile> _file;
  const uchar* _data;
  uint _data_width;
  uint _data_height;
  uint _data_depth;
  QVector3D _voxel_size;
  uint _texture_id;
  QString _description;

  QFuture<void> _preload;
};

#endif
