#ifndef __voltoy_mpritemrenderer_h__
#define __voltoy_mpritemrenderer_h__

#include "MPRItem.h"
#include "MPRItemRendererBase.h"

class MPRItemRenderer : public MPRItemRendererBase {

public:

  MPRItemRenderer();
  ~MPRItemRenderer();

  void synchronize(QQuickFramebufferObject*);

  void render();
  
private:

  void render_mpr();
  void render_outline();

  MPRItem::Plane _plane;
  QColor _outline_color;
  QOpenGLShaderProgram _program_mpr;
  QOpenGLShaderProgram _program_outline;
};

#endif
