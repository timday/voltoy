#include "common.h"

#include "Browser.h"
#include "ImageDisplayItem.h"
#include "MPRItem.h"
#include "MPRsItem.h"
#include "ViewControlItem.h"
#include "VolumeItem.h"
#include "VRItem.h"

int main(int argc,char* argv[]) {
  
  QGuiApplication app(argc,argv);

  const QString appdir=QCoreApplication::applicationDirPath();
  std::cerr << "[Application path " << appdir.toLocal8Bit().data() << "]" << std::endl;
  std::cerr << "[Current working directory " << QDir::currentPath().toLocal8Bit().data() << "]" << std::endl;

#ifdef MACXQUIRKS
  QDir::setCurrent(appdir+QString("/../../"));
  std::cerr << "[Mac: current working directory changed to " << QDir::currentPath().toLocal8Bit().data() << "]" << std::endl;
#else
#ifdef WINQUIRKS
  QDir::setCurrent(appdir);
  std::cerr << "[Windows: current working directory changed to " << QDir::currentPath().toLocal8Bit().data() << "]" << std::endl;
#endif
#endif

  Browser browser;

  qmlRegisterType<ImageDisplayItem>("VolToy",1,0,"ImageDisplay");
  qmlRegisterType<MPRItem>("VolToy",1,0,"MPR");
  qmlRegisterType<MPRsItem>("VolToy",1,0,"MPRs");
  qmlRegisterType<ViewControlItem>("VolToy",1,0,"ViewControl");
  qmlRegisterType<VolumeItem>("VolToy",1,0,"Volume");
  qmlRegisterType<VRItem>("VolToy",1,0,"VR");

  // Wire up quit (enables Qt.quit() in QML); bit odd it needs the cast.
  QObject::connect(reinterpret_cast<QObject*>(browser.engine()),SIGNAL(quit()),&app,SLOT(quit()));

  // Could set an explicit window size
  // (Harmless on iOS which will just fullscreen it anyway)
  browser.resize(QSize(960,540));  // Half of 1920x1080 HD resolution

  browser.setResizeMode(QQuickView::SizeRootObjectToView);

  browser.show();

  if (QGuiApplication::arguments().length()>1 && !QGuiApplication::arguments().last().startsWith('-')) {
    browser.load(QGuiApplication::arguments().last());  // TODO: Loading straight to a vol.qml crashes in OpenGL
  } else {
    browser.load(QUrl::fromLocalFile("qml/splash.qml"));
  }

  return app.exec();
}
