#include "VRItem.h"

#include "VRItemRenderer.h"

VRItem::VRItem(QQuickItem* parent)
  :MPRItemBase(parent)
  ,_normalizedDataToVolumeScaling(0.0,0.0,0.0)
{}

VRItem::~VRItem()
{}

QVector3D VRItem::normalizedDataToVolumeScaling() const {
  return _normalizedDataToVolumeScaling;
}

void VRItem::setNormalizedDataToVolumeScaling(const QVector3D& v) {
  if (v!=_normalizedDataToVolumeScaling) {
    _normalizedDataToVolumeScaling=v;
    emit normalizedDataToVolumeScalingChanged(_normalizedDataToVolumeScaling);
  }
}

QQuickFramebufferObject::Renderer* VRItem::createRenderer() const {
  return new VRItemRenderer();
};
