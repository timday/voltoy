#ifndef __voltoy_mprsitemrenderer_h__
#define __voltoy_mprsitemrenderer_h__

#include "MPRsItem.h"
#include "MPRItemRendererBase.h"

class MPRsItemRenderer : public MPRItemRendererBase {

public:

  MPRsItemRenderer();
  ~MPRsItemRenderer();

  void synchronize(QQuickFramebufferObject*);

  void render();
  
protected:

  // Overridden to get depth attachment
  QOpenGLFramebufferObject* createFramebufferObject(const QSize&);

private:

  void render_mprs();
  void render_outlines();

  QOpenGLShaderProgram _program_mpr;
  QOpenGLShaderProgram _program_outline;
};

#endif
