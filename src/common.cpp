#include "common.h"

qreal minDim(const QVector3D& v) {
  return std::min(v.x(),std::min(v.y(),v.z()));
}

qreal maxDim(const QVector3D& v) {
  return std::max(v.x(),std::max(v.y(),v.z()));
}

std::ostream& operator<<(std::ostream& out,const QVector3D& v) {
  out << "(" << v.x() << "," << v.y() << "," << v.z() << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out,const QVector4D& v) {
  out << "(" << v.x() << "," << v.y() << "," << v.z() << "," << v.w() << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out,const QMatrix4x4& m) {
  out << "(";
  for (uint i=0;i<4;++i) {
    if (i) {
      out << ",";
    }
    out << m.row(i);
  }
  out << ")";
  return out;
}
