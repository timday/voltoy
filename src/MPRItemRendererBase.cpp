#include "MPRItemRendererBase.h"

#include "MPRItemBase.h"

MPRItemRendererBase::MPRItemRendererBase()
  :_width(0.0)
  ,_height(0.0)
  ,_texture_id(0)
  ,_wlWindow(0.0)
  ,_wlLevel(0.0)
{
  _gl.initializeOpenGLFunctions();
}

MPRItemRendererBase::~MPRItemRendererBase()
{}

void MPRItemRendererBase::synchronize(QQuickFramebufferObject* item)
{
  const MPRItemBase*const mpr=dynamic_cast<const MPRItemBase*>(item);
  if (!mpr) {
    std::cerr << "Unexpected argument to MPRItemRendererBase::synchronize" << std::endl;
  }

  _width=mpr->width();
  _height=mpr->height();
  _texture_id=mpr->textureID();
  _transform=mpr->transform();
  _wlWindow=mpr->wlWindow();
  _wlLevel=mpr->wlLevel();

  mpr->synced();

  std::cerr << "!";
}
