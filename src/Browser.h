#ifndef __voltoy_browser_h__
#define __voltoy_browser_h__

#include "FileToucher.h"

// A QQuickView extended to:
// - expose itself in the hosted QML context as "browser".
// - adds a load slot to switch to another QML file.
class Browser : public QQuickView {

  Q_OBJECT

  Q_PROPERTY(QStringList options READ options CONSTANT)

  Q_PROPERTY(QImage capture READ capture WRITE setCapture NOTIFY captureChanged)

public:

  Browser();
  ~Browser();

  Q_INVOKABLE QString fileContents(const QString&) const;

  Q_INVOKABLE void touchFile(const QString&);

  Q_INVOKABLE void cancelAllTouchFiles();

  Q_INVOKABLE QString parameter(const QString&) const;

  Q_INVOKABLE void captureScreenshot();

  const QImage& capture() const;
  void setCapture(const QImage&);

  QStringList options() const;

public slots:

  void fullscreen();

  void openInExternalBrowser(const QUrl&);

  void load(const QUrl&);

  void touchFileFinished();

signals:

  void animationTick();

  void captureChanged(const QImage&);

  void touchedFile(const QString& filename);

private slots:

  void loadImmediate(const QUrl&);

private:

  void setParameters(const QList<QPair<QString, QString> >&);

  QUrl _url;
  QList<QPair<QString, QString> > _url_parameters;
  QHash<QString,QString> _url_parameters_lookup;

  QImage _capture;

  QList<QSharedPointer<FileToucher> > _touchJobs;

  QTimer _animationTicker;
};

#endif
