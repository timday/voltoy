#include "force.h"

namespace {
  volatile int sink;
}

void force(int v) {
  sink=v;
}
