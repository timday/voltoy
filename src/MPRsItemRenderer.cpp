#include "MPRsItemRenderer.h"

#include "MPRsItem.h"

namespace {

  QMatrix4x4 computeZSquash(const QMatrix4x4& t) {

    float zmin=1e30;
    float zmax=-1e30;

    for (int z=0;z<=1;++z) {
      for (int y=0;y<=1;++y) {
	for (int x=0;x<=1;++x) {
	  const QVector3D p=t*QVector3D(x,y,z);
	  zmin=std::min(zmin,p.z());
	  zmax=std::max(zmax,p.z());
	}
      }
    }

    QMatrix4x4 squash;
    squash.scale(1.0,1.0,1.0/(zmax-zmin));
    squash.translate(0.0,0.0,-zmin);

    return squash;
  }
}

MPRsItemRenderer::MPRsItemRenderer()
{
  {
    const bool vshaderok=_program_mpr.addShaderFromSourceCode(
      QOpenGLShader::Vertex,
      "attribute highp vec4 vertex_coord;\n"
      "uniform highp mat4 transform;\n"
      "varying highp vec3 v_texture_coord;\n"
      "void main(void)\n"
      "{\n"
      "  gl_Position = transform*vertex_coord;\n"
      "  v_texture_coord = vec3(vertex_coord);\n"
      "}\n"
    );
    Q_ASSERT(vshaderok);
    
    const bool fshaderok=_program_mpr.addShaderFromSourceCode(
      QOpenGLShader::Fragment,
      "uniform sampler3D texture;\n"
      "uniform highp float wlMin;\n"
      "uniform highp float wlScale;\n"      
      "uniform highp float shading;\n"
      "varying highp vec3 v_texture_coord;\n"
      "void main(void)\n"
      "{\n"
      "  if (v_texture_coord.x<0.0 || v_texture_coord.x>1.0 || v_texture_coord.y<0.0 || v_texture_coord.y>1.0 || v_texture_coord.z<0.0 || v_texture_coord.z>1.0) {\n"
      "    gl_FragColor = vec4(0.0,0.0,0.0,0.0);\n"
      "  } else {\n"
      "    highp float v=texture3D(texture,v_texture_coord).r;\n"
      "    highp float s=shading*max(0.0,min(wlScale*(v-wlMin),1.0));\n"
      "    gl_FragColor = vec4(s,s,s,1.0);\n"
      "  }\n"
      "}\n"
    );
    Q_ASSERT(fshaderok);
    
    const bool linkok=_program_mpr.link();
    Q_ASSERT(linkok);
  }

  {
    const bool vshaderok=_program_outline.addShaderFromSourceCode(
      QOpenGLShader::Vertex,
      "attribute highp vec4 vertex_coord;\n"
      "uniform highp mat4 transform;\n"
      "void main(void)\n"
      "{\n"
      "  gl_Position = transform*vertex_coord;\n"
      "}"
    );
    Q_ASSERT(vshaderok);
    
    const bool fshaderok=_program_outline.addShaderFromSourceCode(
      QOpenGLShader::Fragment,
      "uniform highp vec4 color;\n"
      "void main(void)\n"
      "{\n"
      "  gl_FragColor = color;\n"
      "}"
    );
    Q_ASSERT(fshaderok);
    
    const bool linkok=_program_outline.link();
    Q_ASSERT(linkok);
  }

  glerrcheck(_gl,"MPRsItemRenderer ctor");
}

MPRsItemRenderer::~MPRsItemRenderer()
{}

void MPRsItemRenderer::synchronize(QQuickFramebufferObject* item) {
  MPRItemRendererBase::synchronize(item);
}

void MPRsItemRenderer::render()
{
  // std::cerr << _transform << std::endl;

  glerrcheck(_gl,"MPRsItemRenderer::render start");

  _gl.glEnable(GL_DEPTH_TEST);
  _gl.glDepthMask(GL_TRUE);

  const QColor colour(0,0,0,0);
  _gl.glClearColor(colour.redF(),colour.greenF(),colour.blueF(),colour.alphaF());
  _gl.glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

  render_mprs();
  render_outlines();

  _gl.glDisable(GL_DEPTH_TEST);
  _gl.glDepthMask(GL_FALSE);

  glerrcheck(_gl,"MPRsItemRenderer::render end");
  std::cerr << "[MPRs]";
}

QOpenGLFramebufferObject* MPRsItemRenderer::createFramebufferObject(const QSize& s) {
  QOpenGLFramebufferObject* fbo=QQuickFramebufferObject::Renderer::createFramebufferObject(s);
  fbo->setAttachment(QOpenGLFramebufferObject::Depth);
  return fbo;
}

void MPRsItemRenderer::render_mprs()
{
  const QVector3D pt=_transform*QVector3D(0.0,0.0,0.0);

  std::array<std::vector<QVector3D>,3> geometry;
  geometry[0].push_back(QVector3D(0.0f,0.0f,pt.z()));
  geometry[0].push_back(QVector3D(1.0f,0.0f,pt.z()));
  geometry[0].push_back(QVector3D(1.0f,1.0f,pt.z()));
  geometry[0].push_back(QVector3D(0.0f,0.0f,pt.z()));
  geometry[0].push_back(QVector3D(1.0f,1.0f,pt.z()));
  geometry[0].push_back(QVector3D(0.0f,1.0f,pt.z()));
  
  geometry[1].push_back(QVector3D(0.0f,pt.y(),0.0f));
  geometry[1].push_back(QVector3D(1.0f,pt.y(),0.0f));
  geometry[1].push_back(QVector3D(1.0f,pt.y(),1.0f));
  geometry[1].push_back(QVector3D(0.0f,pt.y(),0.0f));
  geometry[1].push_back(QVector3D(1.0f,pt.y(),1.0f));    
  geometry[1].push_back(QVector3D(0.0f,pt.y(),1.0f));    

  geometry[2].push_back(QVector3D(pt.x(),0.0f,0.0f));
  geometry[2].push_back(QVector3D(pt.x(),1.0f,0.0f));
  geometry[2].push_back(QVector3D(pt.x(),1.0f,1.0f));
  geometry[2].push_back(QVector3D(pt.x(),0.0f,0.0f));
  geometry[2].push_back(QVector3D(pt.x(),1.0f,1.0f));
  geometry[2].push_back(QVector3D(pt.x(),0.0f,1.0f));

  std::array<QVector3D,3> plane_normal={{
      QVector3D(0.0,0.0,1.0),
      QVector3D(0.0,1.0,0.0),
      QVector3D(1.0,0.0,0.0)
    }};

  if (!_program_mpr.bind()) {
    std::cerr << "[Error binding shader program]";
  }

  _gl.glEnable(GL_POLYGON_OFFSET_FILL);
  _gl.glPolygonOffset(1.0,1.0);

  _gl.glEnable(GL_TEXTURE_3D);
  _gl.glBindTexture(GL_TEXTURE_3D,_texture_id);

  QMatrix4x4 aspect;
  aspect.scale(std::max(1.0,_width/_height),std::max(1.0,_height/_width),1.0f);

  QMatrix4x4 actual_transform=(_transform*aspect).inverted();
  actual_transform=computeZSquash(actual_transform)*actual_transform;
    
  const int transform_location=_program_mpr.uniformLocation("transform");
  Q_ASSERT(transform_location!=-1);
  _program_mpr.setUniformValue(transform_location,actual_transform);

  const int vertexCoord_location=_program_mpr.attributeLocation("vertex_coord");
  Q_ASSERT(vertexCoord_location!=-1);
  _program_mpr.enableAttributeArray(vertexCoord_location);

  const int wlMin_location=_program_mpr.uniformLocation("wlMin");
  Q_ASSERT(wlMin_location!=-1);
  _program_mpr.setUniformValue(wlMin_location,float((_wlLevel-0.5*_wlWindow)/65535.0));
  
  const int wlScale_location=_program_mpr.uniformLocation("wlScale");
  Q_ASSERT(wlScale_location!=-1);
  _program_mpr.setUniformValue(wlScale_location,float(1.0/std::max(qreal(1e-6),_wlWindow/65535.0)));
  
  const int shading_location=_program_mpr.uniformLocation("shading");
  Q_ASSERT(shading_location!=-1);

  _gl.glActiveTexture(GL_TEXTURE0);
  const int texture_location=_program_mpr.uniformLocation("texture");
  _program_mpr.setUniformValue(texture_location,0);

  for (int plane=0;plane<3;++plane) {

    {
      // TODO: Has no info about anything but normalized space; needs updating if did.
      // TODO: Needs updating if could ever specify light direction.
      const QVector3D n=(_transform.inverted()).mapVector(plane_normal[plane]).normalized();
      float i=sqrtf((1.0f/16.0f)+(15.0f/16.0f)*(fabsf(n.x()-n.y()-n.z())/sqrtf(3.0)));
      _program_mpr.setUniformValue(shading_location,i);
    }

    _program_mpr.setAttributeArray(vertexCoord_location,&geometry[plane][0]);
    
    _gl.glDrawArrays(GL_TRIANGLES,0,geometry[plane].size());
  }

  _program_mpr.disableAttributeArray(vertexCoord_location);

  _gl.glDisable(GL_POLYGON_OFFSET_FILL);

  _gl.glBindTexture(GL_TEXTURE_3D,0);
  _gl.glDisable(GL_TEXTURE_3D);

  _program_mpr.release();
}

void MPRsItemRenderer::render_outlines()
{
  const QVector3D pt=_transform*QVector3D(0.0,0.0,0.0);

  std::array<std::vector<QVector3D>,3> geometry;
  geometry[0].push_back(QVector3D(0.0f,0.0f,pt.z()));
  geometry[0].push_back(QVector3D(1.0f,0.0f,pt.z()));
  geometry[0].push_back(QVector3D(1.0f,1.0f,pt.z()));
  geometry[0].push_back(QVector3D(0.0f,1.0f,pt.z()));
  geometry[0].push_back(QVector3D(0.0f,0.0f,pt.z()));
  
  geometry[1].push_back(QVector3D(0.0f,pt.y(),0.0f));
  geometry[1].push_back(QVector3D(1.0f,pt.y(),0.0f));
  geometry[1].push_back(QVector3D(1.0f,pt.y(),1.0f));
  geometry[1].push_back(QVector3D(0.0f,pt.y(),1.0f));
  geometry[1].push_back(QVector3D(0.0f,pt.y(),0.0f));    

  geometry[2].push_back(QVector3D(pt.x(),0.0f,0.0f));
  geometry[2].push_back(QVector3D(pt.x(),1.0f,0.0f));
  geometry[2].push_back(QVector3D(pt.x(),1.0f,1.0f));
  geometry[2].push_back(QVector3D(pt.x(),0.0f,1.0f));
  geometry[2].push_back(QVector3D(pt.x(),0.0f,0.0f));

  std::array<QColor,3> outline_color;
  outline_color[0]=QColor(0,0,255);
  outline_color[1]=QColor(0,255,0);
  outline_color[2]=QColor(255,0,0);

  if (!_program_outline.bind()) {
    std::cerr << "[Error binding shader program]";
  }

  _gl.glEnable(GL_LINE_SMOOTH);
  _gl.glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  _gl.glLineWidth(2.0);
  _gl.glEnable(GL_BLEND);
  _gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  QMatrix4x4 aspect;
  aspect.scale(std::max(1.0,_width/_height),std::max(1.0,_height/_width),1.0f);

  QMatrix4x4 actual_transform=(_transform*aspect).inverted();
  actual_transform=computeZSquash(actual_transform)*actual_transform;

  const int transform_location=_program_outline.uniformLocation("transform");
  Q_ASSERT(transform_location!=-1);
  _program_outline.setUniformValue(transform_location,actual_transform);

  const int vertexCoord_location=_program_outline.attributeLocation("vertex_coord");
  Q_ASSERT(vertexCoord_location!=-1);
  _program_outline.enableAttributeArray(vertexCoord_location);

  const int color_location=_program_outline.uniformLocation("color");
  Q_ASSERT(color_location!=-1);

  for (int plane=0;plane<3;++plane) {

    _program_outline.setAttributeArray(vertexCoord_location,&geometry[plane][0]);
    
    _program_outline.setUniformValue(color_location,outline_color[plane]);
    
    _gl.glDrawArrays(GL_LINE_STRIP,0,geometry[plane].size());
  }

  _program_outline.disableAttributeArray(vertexCoord_location);

  _gl.glLineWidth(1.0);
  _gl.glDisable(GL_LINE_SMOOTH);
  _gl.glDisable(GL_BLEND);

  _program_outline.release();
}
