#ifndef __voltoy_volumeitemrenderer_h__
#define __voltoy_volumeitemrenderer_h__

class VolumeItemRenderer : public QQuickFramebufferObject::Renderer {

public:
  VolumeItemRenderer();
  ~VolumeItemRenderer();

  void render();
  
  void synchronize(QQuickFramebufferObject*);

private:

  GLFns _gl;
  GLuint _texture_id;
};

#endif
