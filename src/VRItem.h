#ifndef __voltoy_vritem_h__
#define __voltoy_vritem_h__

#include "MPRItemBase.h"

class VRItem : public MPRItemBase {

  Q_OBJECT

  Q_PROPERTY(QVector3D normalizedDataToVolumeScaling READ normalizedDataToVolumeScaling WRITE setNormalizedDataToVolumeScaling NOTIFY normalizedDataToVolumeScalingChanged) 

public:

  VRItem(QQuickItem* parent=0);
  ~VRItem();

  QVector3D normalizedDataToVolumeScaling() const;
  void setNormalizedDataToVolumeScaling(const QVector3D&);

  QQuickFramebufferObject::Renderer* createRenderer() const;

signals:
  
  void normalizedDataToVolumeScalingChanged(QVector3D);

private:
  
  QVector3D _normalizedDataToVolumeScaling;
};

#endif
