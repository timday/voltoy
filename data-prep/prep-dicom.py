#!/usr/bin/env python

import cPickle as pickle
import gdcm
from multiprocessing import Pool
import numpy as np
import os
import os.path
import sys

import ConvertNumpy

def dot():
    sys.stdout.write('.')
    sys.stdout.flush()

def load_dicom_file(filename):
    # Return (slice array,spacing,origin)
    r=gdcm.ImageReader()
    r.SetFileName(filename)
    r.Read()
    i=r.GetImage()
    s=ConvertNumpy.gdcm_to_numpy(i)
    dot()
    return (s,i.GetSpacing(),i.GetOrigin())

def load_dicom_dir(dir):
    files=[os.path.join(dir,f) for f in reversed(sorted(os.listdir(dir)))]
    pool=Pool()
    slices=pool.map(load_dicom_file,files)
    slices=sorted(slices,key=lambda s: s[2][2],reverse=True)  # Sort by origin z
    zmax=slices[0][2][2]
    zmin=slices[-1][2][2]
    vol=np.zeros((len(slices),slices[0][0].shape[0],slices[0][0].shape[1]),dtype=np.int16)
    for i in xrange(vol.shape[0]):
        vol[i,:,:]=slices[i][0]
    return (
        vol,
        np.array(
            (
                (zmax-zmin)/(vol.shape[0]-1),         # z spacing using DICOM origin
                np.mean([s[1][1] for s in slices]),   # y spacing using DICOM spacing
                np.mean([s[1][0] for s in slices])    # x spacing using DICOM spacing
                )
            )
        )

def dump(v,dstfilenamebase):

    voxel_size_text='{0:.3f}x{1:.3f}x{2:.3f}'.format(
        v[1][0],
        v[1][1],
        v[1][2]
        ).replace('.','_')
    
    filename='{0}-{1}x{2}x{3}-{4}.vol'.format(
        dstfilenamebase,
        v[0].shape[0],
        v[0].shape[1],
        v[0].shape[2],
        voxel_size_text
        )

    print
    print 'File               : {0}'.format(filename)
    print '  Volume size      : {0}'.format(v[0].shape)
    print '  Volume data type : {0}'.format(v[0].dtype)
    print '  Data range       : {0} - {1}'.format(np.min(v[0]),np.max(v[0]))
    print '  Voxel size       : {0}'.format(v[1])

    mmap=np.memmap(filename,mode='w+',shape=v[0].shape,dtype=v[0].dtype)
    mmap[:]=v[0][:]

def main():
    assert len(sys.argv)==3
    srcdirname=sys.argv[-2]
    dstfilenamebase=sys.argv[-1]

    print 'Processing {0}'.format(srcdirname)

    v=load_dicom_dir(srcdirname)

    dump(v,dstfilenamebase+"_full")

    # Halve resolution for low-end graphics
    v=(v[0][::2,::2,::2],2.0*v[1])

    dump(v,dstfilenamebase+"_reduced")

if __name__ == '__main__':
    main()
