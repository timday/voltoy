#!/bin/sh

rm -r -f ../vol/*.vol

./prep-dicom.py ../data/PHENIX/*/COU\ IV ../vol/PHENIX_0
./prep-dicom.py ../data/PHENIX/*/OS      ../vol/PHENIX_1

./prep-dicom.py "../data/ARTIFIX/Thorax 1CTA_THORACIC_AORTA_GATED (Adult)/A Aorta w-c  1.5  B20f  60%" ../vol/ARTIFIX_0

./prep-dicom.py "../data/CARCINOMIX/CT THORACO-ABDO/ARTERIELLES - 5" ../vol/CARCINOMIX_0

./prep-dicom.py "../data/COLONIX/CAT SCAN ABDOMINAL COLONOSCOPIE/abd 1mm - 6"/ ../vol/COLONIX_0

./prep-dicom.py "../data/MANIX/MANIX/CER-CT/ANGIO CT" ../vol/MANIX_0

# Appears to be toxic to GDCM; hangs up
# ./prep-dicom.py "../data/OBELIX/Specials 1_COMBI_CORONARY_SMH/WholeBody  2.0  B20f" ../vol/OBELIX_0
